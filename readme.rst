###################
Rent-a-student
###################

Rent-a-student is een  webapp gemaakt voor het vak  **PHP 1** voor de Thomas More Hogeschool te Mechelen. Het wordt gemaakt door studenten van het tweede jaar **Interactive Multimedia Design**.


*******************
Gemaakt door:
*******************

* Lucas Poignonnec
* Stijn D'Hollander
* Yoshi Mannaert
* Andreas Busschop


**************************
Beschrijving
**************************

Studenten uit het secundair onderwijs kunnen open lesdagen bijwonen
in IMD. Hiervoor is een applicatie nodig waarmee deze studenten een
IMD’er kunnen selecteren op een bepaalde dag om te volgen binnen onze
opleiding. Onze eigen studenten kunnen zo vanuit hun eigen verhaal
het echte IMD tonen aan nieuwe studenten.
Het project is geslaagd wanneer aan onderstaande voorwaarden is
voldaan. Let op, dit zijn minimum­vereisten. Heb je ideeën voor
betere of extra features, dan mag je die zeker inbouwen.



*******
Functionele vereisten
*******

* Admin 
   * moet data kunnen toevoegen en verwijderen waarop studenten “gehuurd” kunnen worden. Testdata kunnen jullie hier vinden: http://openles.thomasmore.be/defaultDatum.asp
   * moet boekingen kunnen bekijken (wie heeft welke student wanneer geboekt)
   * moeten makkelijk een lijst kunnen opvragen met studenten die een bezoek aan de opleiding hebben ingepland. Zorg voor een eenvoudige lijst met email, tijdstip, naam en voornaam. Zo kunnen deze studenten aangeschreven worden via mailchimp
   * moet bijkomende admin accounts kunnen toevoegen of verwijderen
* Gidsen/meters/peters (imd studenten)
   * Moeten een profiel kunnen aanmaken: foto, naam, korte bio (waarom voor imd gekozen), interessegebied (design of dev), aangeven of je 1, 2 of 3IMD student bent, stad, email, wachtwoord,...
   * Moet eigen profiel kunnen aanpassen (gegevens + foto + wachtwoord wijzigen)
* Bezoekers
   * Facebook login is nodig voordat een student geboekt kan worden (anders riskeren we teveel fake bookings)
   * Die moeten op de app een gids/meter/peter kunnen kiezen
   * Een datum en uur kiezen voor het bezoek (admin stelt mogelijkheden in)
   * In contact komen via berichten in de applicatie met hun gekozen imd gids, afspraken kunnen maken (waar afspreken voor het bezoek, lunch meebrengen of niet...)
   *  Laat de app een notification mailen naar de IMD’er die een nieuw bericht heeft ontvangen. Beantwoorden kan via de app
   * Moeten kunnen zien welke gidsen er nog beschikbaar zijn voor welke data. Zodra een datum+tijdstip geselecteerd wordt krijg je direct feedback over beschikbaarheid.
   * Wie is de populairste gids? Werk met ratings. Als er nog geen ratings zijn kan je kijken naar welke gids de meeste boekings heeft.
   *  Laat de student feedback geven. Was het een zinvolle dag? Zorg dat admins deze feedback kunnen inzien en erop kunnen antwoorden (mag via mail)
   *  Zodra een bezoek is afgelopen (tijdstip is voorbij) stuur je automatisch een mailtje naar de student met de vraag om de gids te raten en een korte quote (max 120 karakters) achter te laten die de dag beschrijft. Deze quotes toon je random op de homepage.
   * Motiveer studenten om via instagram foto’s te posten van de meeloopdag met de hashtags #WeAreIMD #openlesdag
* Algemeen
   * Share mogelijkheden voor social media
   * Goede copy, vertel een verhaal

**************************
Technische Vereisten
**************************

* werk van in het begin met GIT (zoek een IMD’er die het gebruik hiervan kan uitleggen indien nodig of bekijk tutorials!)
* het project moet online staan, zoek zelf een gratis of goedkope hosting
* gebruik AJAX doorheen uw applicatie op een correcte manier en zorg dat het gebruik hiervan een duidelijke meerwaarde heeft voor uw applicatie (gebruik jQuery, JS of Angular.js om uw AJAX­calls uit te voeren)
* werk via de principes van OOP, maak correcte klasses aan, bij twijfel bespreek je je aanpak op voorhand met je docent
* als je gebruikers een nieuwe account laat maken, zorg dan voor veilig encryptie van wachtwoorden via bcrypt (geen md5+salt) !
* optioneel: als je een framework gebruikt, maak dan op dit moment de keuze voor Code Igniter. Er zijn modernere en complexere frameworks, maar die gebruiken we in PHP2, start met de basis om het MVC­patroon onder de knie te krijgen
* GIT commits op eigen naam + Scrumwise = individueel bewijs van opgeleverd werk (zet je een SCRUM taak op “done” maar is die niet klaar, dan krijg je stevige strafpunten)