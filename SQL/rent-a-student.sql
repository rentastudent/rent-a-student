-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 18 mei 2015 om 23:04
-- Serverversie: 5.6.17
-- PHP-versie: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `rent-a-student`
--
CREATE DATABASE IF NOT EXISTS `rent-a-student` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `rent-a-student`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `admin_gebruiker`
--

CREATE TABLE IF NOT EXISTS `admin_gebruiker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voornaam` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `achternaam` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailadres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paswoord` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Tabel leegmaken voor invoegen `admin_gebruiker`
--

TRUNCATE TABLE `admin_gebruiker`;
--
-- Gegevens worden geëxporteerd voor tabel `admin_gebruiker`
--

INSERT INTO `admin_gebruiker` (`id`, `voornaam`, `achternaam`, `emailadres`, `paswoord`) VALUES
(4, 'Stijn', 'D''Hollander', 's_dhollander@hotmail.com', '$2y$10$0yWBEW5pKcmca9OJB7zfquamS8rggjQqWpCIOoabTWBq1vh1ZCM7S'),
(7, 'yoshi', 'mannaert', 'yoshi.mannaert@gmail.com', '$2y$10$XrjYnJc9tp3FiCZX5N9O7uPPXgNn3iU0PGiEjHqbW0pHbIQycUjW.'),
(8, 'Andreas', 'Busschop', 'andreas.busschop@gmail.com', '$2y$10$yZloAFt/K3cgNiBcugy5zO15.8W6kFj1n6IHj8UCKMMoMZlcPDzeu'),
(9, 'admin', 'admin', 'thomasmorerentastudent@gmail.com', '$2y$10$iCPV6vDemW3VoAtIlEd3SunA57L2RlEm1ChYsft8H.E72wTKUhn7a');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `berichten`
--

CREATE TABLE IF NOT EXISTS `berichten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chatId` int(11) NOT NULL,
  `bericht` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `zenderId` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ontvangerId` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `uur` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `datum` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Tabel leegmaken voor invoegen `berichten`
--

TRUNCATE TABLE `berichten`;
--
-- Gegevens worden geëxporteerd voor tabel `berichten`
--

INSERT INTO `berichten` (`id`, `chatId`, `bericht`, `zenderId`, `ontvangerId`, `uur`, `datum`) VALUES
(3, 80, 'jwo', '12', '828165653937245', '1431946319', '04-18-15'),
(4, 80, 'jow', '828165653937245', '12', '1431946565', '05-18-15'),
(5, 80, 'jow', '828165653937245', '12', '1431946570', '05-18-15'),
(6, 80, 'hallo', '828165653937245', '12', '1431946572', '05-18-15'),
(7, 80, 'hoe gaat ie', '12', '828165653937245', '1431946578', '05-18-15'),
(8, 80, 'jow', '828165653937245', '12', '1431946581', '05-18-15'),
(9, 80, 'hallo', '828165653937245', '12', '1431946584', '04-18-15'),
(10, 80, 'jow', '12', '828165653937245', '1431946596', '05-18-15'),
(11, 80, 'jow', '12', '828165653937245', '1431946612', '05-18-15'),
(12, 80, 'hallo', '828165653937245', '12', '1431946618', '05-18-15'),
(13, 80, 'jow', '12', '828165653937245', '1431946668', '05-18-15'),
(14, 80, 'how', '828165653937245', '12', '1431946677', '05-18-15'),
(15, 80, 'jo', '828165653937245', '12', '1431946702', '05-18-15'),
(16, 80, 'helo', '828165653937245', '12', '1431946706', '05-18-15'),
(17, 80, 'fggdb', '828165653937245', '12', '1431946710', '05-18-15'),
(18, 80, 'jow', '828165653937245', '12', '1431946730', '05-18-15'),
(19, 80, 'hallo', '828165653937245', '12', '1431946733', '05-18-15'),
(20, 80, 'jow', '12', '828165653937245', '1431946753', '05-18-15'),
(21, 80, 'fqqs', '828165653937245', '12', '1431946764', '05-18-15'),
(22, 80, 'jow', '828165653937245', '12', '1431946783', '05-18-15'),
(23, 80, 'hallo', '828165653937245', '12', '1431946796', '05-18-15'),
(24, 80, 'jow', '12', '828165653937245', '1431946813', '05-18-15'),
(25, 80, 'jow', '12', '828165653937245', '1431946971', '05-18-15'),
(26, 80, 'jow', '828165653937245', '12', '1431946984', '05-18-15'),
(27, 80, 'jow', '828165653937245', '12', '1431946990', '05-18-15'),
(28, 80, 'hu', '828165653937245', '12', '1431947002', '05-18-15'),
(29, 80, 'hu', '828165653937245', '12', '1431947006', '05-18-15'),
(30, 80, 'hdsmlkgq', '828165653937245', '12', '1431947030', '05-18-15'),
(31, 80, 'jow', '828165653937245', '12', '1431948104', '05-18-15'),
(32, 80, 'jow', '828165653937245', '12', '1431948568', '05-18-15'),
(33, 80, 'zha', '828165653937245', '12', '1431948633', '05-18-15'),
(34, 80, 'qf', '828165653937245', '12', '1431948704', '05-18-15'),
(35, 80, 'jow', '828165653937245', '12', '1431949204', '05-18-15'),
(36, 80, 'sqfdqs', '828165653937245', '12', '1431949417', '05-18-15'),
(37, 80, 'fsdqsg', '828165653937245', '12', '1431949513', '05-18-15'),
(38, 80, 'sqff', '828165653937245', '12', '1431949563', '05-18-15'),
(39, 80, 'sfqfsq', '828165653937245', '12', '1431949564', '05-18-15'),
(40, 80, 'dqdg', '828165653937245', '12', '1431950238', '05-18-15'),
(41, 80, 'vwxv', '828165653937245', '12', '1431950261', '05-18-15'),
(42, 80, 'ghgh', '828165653937245', '12', '1431950435', '05-18-15'),
(43, 80, 'jow', '828165653937245', '12', '1431950678', '05-18-15'),
(44, 80, 'sqf', '828165653937245', '12', '1431950889', '05-18-15'),
(45, 80, 'fqs', '828165653937245', '12', '1431950971', '05-18-15'),
(46, 80, 'sfssq', '828165653937245', '12', '1431951110', '05-18-15'),
(47, 80, 'jow', '828165653937245', '12', '1431951346', '05-18-15'),
(48, 80, 'fsqsf', '828165653937245', '12', '1431951714', '05-18-15'),
(49, 80, 'sffqs', '828165653937245', '12', '1431951917', '05-18-15'),
(50, 80, 'jo', '828165653937245', '12', '1431955056', '05-18-15'),
(51, 80, 'halllo', '828165653937245', '12', '1431955186', '05-18-15'),
(52, 80, 'jow', '828165653937245', '12', '1431955872', '05-18-15'),
(53, 80, 'sfqq', '828165653937245', '12', '1431956008', '05-18-15'),
(54, 80, 'ngnngf', '828165653937245', '12', '1431956186', '05-18-15'),
(55, 80, 'jyr', '828165653937245', '12', '1431957165', '05-18-15'),
(56, 81, 'jow', '828165653937245', '77', '1431980378', '05-18-15'),
(57, 81, 'jow', '77', '828165653937245', '1431980962', '05-18-15');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bezoeken`
--

CREATE TABLE IF NOT EXISTS `bezoeken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `uur` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `IMDStudentId` int(11) NOT NULL,
  `BezoekerId` int(11) NOT NULL,
  `Notified` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Tabel leegmaken voor invoegen `bezoeken`
--

TRUNCATE TABLE `bezoeken`;
--
-- Gegevens worden geëxporteerd voor tabel `bezoeken`
--

INSERT INTO `bezoeken` (`id`, `datum`, `uur`, `IMDStudentId`, `BezoekerId`, `Notified`) VALUES
(36, '2015-04-02', '9', 1, 51, 1),
(37, '2015-04-25', '13', 2, 53, 1),
(38, '2015-10-02', '15', 11, 53, 0),
(39, '2018-03-03', '11', 9, 53, 0),
(40, '2015-04-07', '15', 7, 54, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bezoeker`
--

CREATE TABLE IF NOT EXISTS `bezoeker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voornaam` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `achternaam` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `fbuserid` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=63 ;

--
-- Tabel leegmaken voor invoegen `bezoeker`
--

TRUNCATE TABLE `bezoeker`;
--
-- Gegevens worden geëxporteerd voor tabel `bezoeker`
--

INSERT INTO `bezoeker` (`id`, `voornaam`, `achternaam`, `email`, `fbuserid`) VALUES
(62, 'Andreas', 'Busschop', 'andreas.experiment@live.be', '828165653937245');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `chatId` int(11) NOT NULL AUTO_INCREMENT,
  `bezoekerfbId` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `studentId` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`chatId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

--
-- Tabel leegmaken voor invoegen `chat`
--

TRUNCATE TABLE `chat`;
--
-- Gegevens worden geëxporteerd voor tabel `chat`
--

INSERT INTO `chat` (`chatId`, `bezoekerfbId`, `studentId`) VALUES
(80, '828165653937245', '12'),
(81, '828165653937245', '77');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Tabel leegmaken voor invoegen `data`
--

TRUNCATE TABLE `data`;
--
-- Gegevens worden geëxporteerd voor tabel `data`
--

INSERT INTO `data` (`id`, `datum`) VALUES
(1, '2015-04-02'),
(2, '2015-03-07'),
(3, '2015-04-05'),
(4, '2015-06-06'),
(5, '2017-02-02'),
(6, '2018-03-03'),
(7, '2016-04-04'),
(8, '2015-05-15'),
(9, '2015-04-24'),
(10, '2015-04-16'),
(11, '2015-04-25'),
(12, '2015-04-18'),
(13, '2015-04-23'),
(14, '2015-10-02');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `datagidsen`
--

CREATE TABLE IF NOT EXISTS `datagidsen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DatumId` int(11) NOT NULL,
  `IMDStudentId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Tabel leegmaken voor invoegen `datagidsen`
--

TRUNCATE TABLE `datagidsen`;
--
-- Gegevens worden geëxporteerd voor tabel `datagidsen`
--

INSERT INTO `datagidsen` (`id`, `DatumId`, `IMDStudentId`) VALUES
(1, 7, 1),
(2, 8, 1),
(3, 6, 1),
(4, 9, 1),
(5, 7, 1),
(6, 10, 1),
(7, 1, 12),
(8, 7, 12),
(9, 11, 12),
(10, 12, 12),
(11, 5, 12),
(12, 6, 12),
(13, 12, 12),
(14, 4, 12),
(15, 8, 12),
(16, 10, 12);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `feedbackbezoeker`
--

CREATE TABLE IF NOT EXISTS `feedbackbezoeker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bezoekerId` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `sfeer` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `indruk` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `aanraden` tinyint(1) NOT NULL,
  `verbeteringen` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `positief` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `toestemming` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Tabel leegmaken voor invoegen `feedbackbezoeker`
--

TRUNCATE TABLE `feedbackbezoeker`;
--
-- Gegevens worden geëxporteerd voor tabel `feedbackbezoeker`
--

INSERT INTO `feedbackbezoeker` (`id`, `bezoekerId`, `sfeer`, `contact`, `indruk`, `aanraden`, `verbeteringen`, `positief`, `toestemming`) VALUES
(1, '51', '5', '2', '4', 0, 'Moeilijke communicatie met de gids. Hij checkte zijn berichten niet via de applicatie. Verder was het moeilijk om hem in te schatten als persoon.', 'Er heerst een heel leuke sfeer op de campus. Toffe lectoren en en een heel open sfeer.', 0),
(2, '53', '5', '2', '4', 0, 'Moeilijke communicatie met de gids. Hij checkte zijn berichten niet via de applicatie. Verder was het moeilijk om hem in te schatten als persoon.', 'Er heerst een heel leuke sfeer op de campus. Toffe lectoren en en een heel open sfeer.', 0),
(3, '53', '5', '3', '5', 0, 'Test', 'Test 2', 0),
(7, '54', 'Zeer goed', 'Zeer goed', 'Zeer goed', 0, 'zdqzdzdqdqzzqddzqdqz', 'qdzdqzdzqdzqdqzqdzdqz', 0),
(8, '54', 'Zeer goed', 'Zeer slecht', 'Zeer slecht', 1, 'Alles', 'Niks', 1),
(9, '61', 'Goed', 'Ok', 'Ok', 1, 'fssqfq', 'sqfsqfsqfsqf', 1),
(10, '61', 'Goed', 'Slecht', 'Ok', 0, 'fqssqfqs', 'sqffsqfsqfqs', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `profielen`
--

CREATE TABLE IF NOT EXISTS `profielen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voornaam` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `achternaam` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `paswoord` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `studiejaar` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `afstudeerrichting` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `telefoonnummer` int(200) DEFAULT NULL,
  `woonplaats` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `motivatie` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `padProfiel` varchar(600) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci AUTO_INCREMENT=80 ;

--
-- Tabel leegmaken voor invoegen `profielen`
--

TRUNCATE TABLE `profielen`;
--
-- Gegevens worden geëxporteerd voor tabel `profielen`
--

INSERT INTO `profielen` (`id`, `voornaam`, `achternaam`, `email`, `paswoord`, `studiejaar`, `afstudeerrichting`, `telefoonnummer`, `woonplaats`, `motivatie`, `padProfiel`) VALUES
(77, 'Andreas', 'Busschop', 'andreas.experiment@live.be', '$2y$10$5PA6V0K2Vj4zhWGfcHGS9.rp2vZAvmhx3wmvsd2ctorNqiPFgMwoi', '1', 'webontwikkeling', 564523, 'Mechelen', 'Ik heb gekozen voor IMD omdat ik wat meer wilde leren over IMD.', '143198033120130818_220830044_iOS.jpg'),
(78, 'Lucas', 'Poignonnec', 'lucas@gmail.com', '$2y$10$5BXHS8iAy0Y4PVEfYmp1aOSvOoqVn7Uu6yLimlnWWfTQqESLJI6Fi', '1', 'webontwikkeling', 56165132, 'Rumst', 'Hallo, ik vind dit leuk.', '143198291011150372_978138485543266_4720210894944969733_n.jpg'),
(79, 'stijn', 'dhollander', 'stijn@student.thomasmore.be', '$2y$10$ZkuxxRcT6XvqzQHJkvHzleDAwYB.L/czmtLudG8Dv2bcZAS3VCqtu', '3', 'webdesign', 621654123, 'Antwerpen', 'Ik wilde wat leren tekenen.', '1431982977295326_10200540739632967_135204869_n.jpg');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ratinggidsen`
--

CREATE TABLE IF NOT EXISTS `ratinggidsen` (
  `ratingGidsId` int(11) NOT NULL AUTO_INCREMENT,
  `BezoekId` int(1) NOT NULL,
  `IMDStudentId` int(1) NOT NULL,
  `rating` int(1) NOT NULL,
  `quote` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ratingGidsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Tabel leegmaken voor invoegen `ratinggidsen`
--

TRUNCATE TABLE `ratinggidsen`;
--
-- Gegevens worden geëxporteerd voor tabel `ratinggidsen`
--

INSERT INTO `ratinggidsen` (`ratingGidsId`, `BezoekId`, `IMDStudentId`, `rating`, `quote`) VALUES
(1, 36, 1, 5, 'Don''t cry because it''s over, smile because it happened.'),
(5, 41, 2, 4, 'Be yourself; everyone else is already taken.'),
(6, 40, 2, 4, 'Be who you are and say what you feel, because those who mind don''t matter'),
(7, 43, 2, 3, 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe.'),
(8, 42, 3, 2, 'Be the change that you wish to see in the world.'),
(10, 45, 3, 1, 'In three words I can sum up everything I''ve learned about life: it goes on.'),
(11, 46, 3, 1, 'No one can make you feel inferior without your consent.'),
(12, 47, 4, 1, 'A friend is someone who knows all about you and still loves you.'),
(13, 48, 5, 4, 'Live as if you were to die tomorrow. Learn as if you were to live forever.'),
(14, 49, 5, 4, 'I am so clever that sometimes I don''t understand a single word of what I am saying. '),
(15, 39, 9, 3, 'Quote test');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
