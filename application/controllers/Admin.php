<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller
{
	public function index()
	{
		echo "Index";
	}
	public function datumToevoegen()
	{
		$this->load->helper('date');
		
		//model klasse laden
		$this->load->model("Dates_model", '', true);

		//als er een POST request is
		if(isset($_POST['toevoegen']))
		{
			//input uit form lezen
			$datum = $this->input->post('date');
			
			//maak dan een neuw object Dates_model()
			$d = new Dates_model();
			$d->datum = $datum;

			//save() via het model naar de db
			$this->Dates_model->save($d);
		}
		$this->load->model("Dates_model", '', true);
		if (isset($_POST['verwijder'])) {
			$datumId = $this->input->post('datumId');

            $dd = new Dates_model();
            $dd->id = $datumId;
            $this->Dates_model->deleteDate($dd);
        }

		$data = [
			"hello"  => "world",
			"dates" => $this->Dates_model->get_all()
		];
		//surfen naar /Date/datumToevoegen
		$this->load->view('admin/datumToevoegen', $data);
	}

	public function datumVerwijderen($id)
	{
		$this->load->model("Dates_model", '', true);
		$d = new Dates_model();

		$delete = $d->delete($id);
		$deleted = $d->getOne($id);
		echo json_encode($deleted);
	}

	public function bezoekerLijst()
	{
		$this->load->model("Boeking_model", '', true);
		

		$boekingen_arr = $this->Boeking_model->getAllBoekingen();
		$this->load->view('admin/bezoekerLijst', $boekingen_arr);
	}

	public function boekingen()
	{
		
		$this->load->model("Boeking_model", '', true);
		

		$boekingen_arr = $this->Boeking_model->getAllBoekingen();
		$this->load->view('admin/boekingen', $boekingen_arr);
		//print_r($boekingen_arr);
		/*$this->load->model("Bezoek_model", '', true);
		$data = [
			"hello"  => "world",
			"bezoeken" => $this->Bezoek_model->get_all()
		];
		$this->load->view('admin/bezoekerLijst', $data);*/
	}
	

	public function registratie()
    {
    	
    		$this->load->library('session');
        	if($this->session->userdata('isAdmin') != true)
        	{
        		$this->load->view('errors/loginerror');
        	} else {
        		$this->load->model('Adminregistratie_model', '', true);
                $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');

                if (isset($_POST['verwijder'])) {
                 	$this->form_validation->set_rules('v_emailadres', 'vE-mail', 'trim|required|valid_email');
                } else if (isset($_POST['registreer'])) {
                	$this->form_validation->set_rules('voornaam', 'Voornaam', 'trim|required');
                	$this->form_validation->set_rules('achternaam', 'Achternaam', 'trim|required');
               		$this->form_validation->set_rules('emailadres', 'E-mail', 'trim|required|valid_email|is_unique[admin_gebruiker.emailadres]', 
                        array('required' => 'De mail moet een schoolmail zijn.')
                	);
					$this->form_validation->set_rules('paswoord', 'Paswoord', 'trim|required|min_length[6]');
					$this->form_validation->set_rules('bevpaswoord', 'Bevestiging paswoord', 'trim|required|matches[paswoord]|min_length[6]');
                }
                
				

                if ($this->form_validation->run() == FALSE)
                {
                    $this->load->view('admin/registratie');
                }
                else
                {
                	if (isset($_POST['verwijder'])) {
                		$va = new Adminregistratie_model();
                		$va->VerwijderEmail = $this->input->post('v_emailadres');
                		$flag = $va->deleteAdmin();
                		if ($flag == 'true') {
                			$this->load->view('admin/registratie_succes');
                		} else {
                			$this->load->view('admin/registratie');
                		}

                	} else if (isset($_POST['registreer'])) {
                		$Voornaam = $this->input->post('voornaam');
						$Achternaam = $this->input->post('achternaam');
						$Email = $this->input->post('emailadres');
						$Paswoord = $this->input->post('paswoord');
					
						// maak dan een nieuw object Adminregistratie_model()
						$ar = new Adminregistratie_model();
						$ar->Voornaam = $Voornaam;
						$ar->Achternaam = $Achternaam;
						$ar->Email = $Email;
						$ar->Paswoord = $Paswoord;
						
						//save() via het model naar de db
						$ar->save();

            			$this->session->set_flashdata('admin_register', 'Het admin-account is geregistreerd!');
            			redirect('admin/home');
                	}
		        }
		    }
                
    }

    public function feedback()
    {
    	
    		$this->load->library('session');
        	if($this->session->userdata('isAdmin') != true)
        	{
        		$this->load->view('errors/loginerror');
        	} 
        	else 
        	{
        		$this->load->model('Feedback_model', '', true);
                $feedback = [
					"feedback" => $this->Feedback_model->get_all()
				];
				
				$this->load->view('admin/feedback', $feedback);
			
		    }
	}

	public function home()  {
    $this->load->library('session');
        if($this->session->userdata('isAdmin') != true)
        {
            $this->load->view('errors/loginerror');     
        } else { 
            $this->load->view('admin/home');
        }
    }

	public function emailNaarBezoeker()
	{

		$this->load->model('Admin_model', '', true);
		$dataMails = $this->Admin_model->checkDate();
		foreach ($dataMails as $bezoek) {
			$ci = get_instance();
			$ci->load->library('email');
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "smtp.mandrillapp.com"; //Wachtwoord Mandrill = Rentastudent!
			$config['smtp_port'] = "587";
			$config['smtp_user'] = "thomasmorerentastudent@gmail.com"; 
			$config['smtp_pass'] = "exZVeV8oBPyqZ-DCr0XCTQ"; //API KEY MANDRILL
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";

			$ci->email->initialize($config);

			$ci->email->from('thomasmorerentastudent@gmail.com', 'RentAStudent');
			$list = array($bezoek['email']);
			$ci->email->to($list);
			$this->email->reply_to('thomasmorerentastudent@gmail.com', 'RentAStudent');
			$ci->email->subject('Beoordeling Gids Bezoek IMD-campus');
			$ci->email->message('Beste '.$bezoek['voornaam'].', gelieve op volgende link te klikken om een rating geven aan de gids die je begeleidde tijdens je bezoek aan de IMD-campus.
			 <a href="http://localhost/rent-a-student/index.php/bezoeker/gidsBeoordelen/'.$bezoek['id'].'">http://localhost/rent-a-student/index.php/bezoeker/gidsBeoordelen/'.$bezoek['id'].'</a>');
			$ci->email->send();

			echo "Mail verzonden naar bezoekerID".$bezoek['id'];

			$updateNotified = $this->Admin_model->updateNotified($bezoek['id']);
		}

	}
                
}
