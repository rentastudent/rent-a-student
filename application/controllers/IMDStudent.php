<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IMDStudent extends CI_Controller 
{
    
    public function registratie()  {
        $this->load->library('session');
        $this->load->model("Student_model", '', true); 

            //Form validation ZONDER UPLOAD
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[profielen.email]');
        $this->form_validation->set_message('is_unique', 'Dit emailadres is al geregistreerd. Vul een ander email in');  

            //Controleren of de form validation gelukt is, indien false gaat hij de form opnieuw laden en de ingevulde data terug in de velden              steken.

        if ($this->form_validation->run() == TRUE)
        {
            if($this->input->server('REQUEST_METHOD') == 'POST') 
            {  
                //Object aanmaken en de input uit de form lezen.
                $voornaam = $this->input->post('voornaam');
                $achternaam = $this->input->post('achternaam');
                $email = $this->input->post('email');
                $paswoord = $this->input->post('paswoord');
                $studiejaar = $this->input->post('studiejaar');
                $afstudeerrichting = $this->input->post('afstudeerrichting');
                $telefoonnummer = $this->input->post('telefoonnummer');
                $woonplaats = $this->input->post('woonplaats');
                $motivatie = $this->input->post('motivatie');

                $s = new Student_model();
                $s->voornaam = $voornaam;
                $s->achternaam = $achternaam;
                $s->email = $email;
                $s->paswoord = $paswoord;
                $s->studiejaar = $studiejaar;
                $s->afstudeerrichting = $afstudeerrichting;
                $s->telefoonnummer = $telefoonnummer;
                $s->woonplaats = $woonplaats;
                $s->motivatie = $motivatie;

                    //File upload CODE
                $old_name                       = $_FILES["userfile"]['name'];
                $new_name                       = time().$_FILES["userfile"]['name'];
                $config['file_name']            = $new_name;
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 3000;
                $config['max_width']            = 4000;
                $config['max_height']           = 4000;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('profiel_error', 'Upload mislukt!');
                    $this->load->view('IMDStudent/registratie', $error);
                }
                else
                {
                    //$pad = $this->upload->data('file_name');
                    $upload_data = $this->upload->data();
                    $resize['image_library'] = 'gd2';
                    $resize['source_image'] = $upload_data["full_path"];
                    $resize['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                    $resize['create_thumb'] = FALSE;
                    $resize['maintain_ratio'] = FALSE;
                    $resize['width']         = 860;
                    $resize['height']       = 860;
                    $dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
                    $resize['master_dim'] = ($dim > 0)? "height" : "width";

                    //$this->load->library('image_lib', $resize);
                    $this->load->library('image_lib');
                    $this->image_lib->initialize($resize);

                    if ( ! $this->image_lib->resize())
                    {
                        echo $this->image_lib->display_errors();
                    }

                    else 
                    {
                        //$pad = $this->upload->data('file_name');
                        //$this->image_lib->clear();

                        $crop['image_library'] = 'gd2';
                        $crop['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                        $crop['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                        $crop['create_thumb'] = FALSE;
                        $crop['maintain_ratio'] = FALSE;
                        $crop['width']         = 430;
                        $crop['height']       = 430;
                        $crop['x_axis'] = 215;
                        $crop['y_axis'] = 215;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($crop); 
                        if ( ! $this->image_lib->crop())
                        {
                            echo $this->image_lib->display_errors();
                        }

                        else {
                        $pad = $upload_data["file_name"];
                        //unlink($upload_data["full_path"]);
                        $s->padProfiel = $pad;
                        $s->save();
                        $this->session->set_flashdata('success', 'Je account is geregistreerd, je kan nu inloggen.');
                        redirect('gebruiker/login');
                         }
                    }
                }                                   
            }                 
        } 
        else
        {
            $this->load->view("IMDStudent/registratie");
        }        
            //if($this->input->post('upload')){           
    }
    
    public function profielAanpassen()  
    {

            //Uitlezen van de data uit de databank en doorgeven aan de view.

        $this->load->model("Student_model", '', true);
        $this->load->helper("file");
        $this->load->library('session');
        $data = [ 
        "profiel" => $this->Student_model->getProfileData()
        ];
            //Pad naar foto halen uit de array zodat we die kunnen verwijderen als er een nieuwe foto gekozen wordt.
        $fotoURL = $data['profiel']['0']['padProfiel'];

            //Checken of er een sessie IMDStudent is

        if($this->session->userdata('isStudent') != true)
        {
            $this->load->view('errors/loginerror');     
        }
        else 
        {
            $this->load->view('IMDStudent/profielUpdate', $data);
        }

            //Form controleren, uitlezen en tabel updaten.
        if($this->input->server('REQUEST_METHOD') == 'POST') 
        {  
                        //input uit de form lezen.
            $voornaam = $this->input->post('voornaam');
            $achternaam = $this->input->post('achternaam');
            $email = $this->input->post('email');
            $huidigPaswoord = $this->input->post('huidigPaswoord');
            $nieuwPaswoord = $this->input->post('nieuwPaswoord');
            $confirmPaswoord = $this->input->post('confirmPaswoord');
            $studiejaar = $this->input->post('studiejaar');
            $telefoonnummer = $this->input->post('telefoonnummer');
            $afstudeerrichting = $this->input->post('afstudeerrichting');
            $woonplaats = $this->input->post('woonplaats');
            $motivatie = $this->input->post('motivatie');

            $s = new Student_model();
            $s->voornaam = $voornaam;
            $s->achternaam = $achternaam;
            $s->email = $email;
            $s->studiejaar = $studiejaar;
            $s->telefoonnummer = $telefoonnummer;
            $s->afstudeerrichting = $afstudeerrichting;
            $s->woonplaats = $woonplaats;
            $s->motivatie = $motivatie;
            $s->padProfiel = $fotoURL;
            
            if (!empty($huidigPaswoord) || !empty($nieuwPaswoord) || !empty($confirmPaswoord)) 
            {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('huidigPaswoord', 'Huidig Paswoord', 'trim|required');
                $this->form_validation->set_rules('nieuwPaswoord', 'Nieuw Paswoord', 'trim|required|min_length[5]|max_length[12]');
                $this->form_validation->set_rules('confirmPaswoord', 'Confirmeer Paswoord', 'trim|required|matches[nieuwPaswoord]');

                if ($this->form_validation->run() == TRUE)
                {

                    if ($this->Student_model->controleerHuidigPaswoord($huidigPaswoord)) 
                    {

                        $s->paswoord = $nieuwPaswoord;

                    }

                    else {

                        $this->session->set_flashdata('profiel_error', 'Huidig wachtwoord klopt niet!');
                        redirect('IMDStudent/profielAanpassen');

                    }

                }

                else {

                    $this->session->set_flashdata('profiel_error', 'Je hebt één van de drie velden voor het veranderen van je wachtwoord verkeerd ingevuld!');
                    redirect('IMDStudent/profielAanpassen');

                }

            }
            
            if(isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0)
            {
                                   //File upload CONFIG

                $old_name                       = $_FILES["userfile"]['name'];
                $new_name                       = time().$_FILES["userfile"]['name'];
                $config['file_name']            = $new_name;
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 3000;
                $config['max_width']            = 4000;
                $config['max_height']           = 4000;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('profiel_error', 'Upload mislukt!');
                    $this->load->view('IMDStudent/registratie', $error);
                }
                else
                {
                    $upload_data = $this->upload->data();
                    $resize['image_library'] = 'gd2';
                    $resize['source_image'] = $upload_data["full_path"];
                    $resize['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                    $resize['create_thumb'] = FALSE;
                    $resize['maintain_ratio'] = FALSE;
                    $resize['width']         = 860;
                    $resize['height']       = 860;
                    $dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
                    $resize['master_dim'] = ($dim > 0)? "height" : "width";

                    //$this->load->library('image_lib', $resize);
                    $this->load->library('image_lib');
                    $this->image_lib->initialize($resize);

                    if ( ! $this->image_lib->resize())
                    {
                        echo $this->image_lib->display_errors();
                    }

                    else 
                    {
                        $crop['image_library'] = 'gd2';
                        $crop['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                        $crop['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                        $crop['create_thumb'] = FALSE;
                        $crop['maintain_ratio'] = FALSE;
                        $crop['width']         = 430;
                        $crop['height']       = 430;
                        $crop['x_axis'] = 215;
                        $crop['y_axis'] = 215;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($crop); 
                        if ( ! $this->image_lib->crop())
                        {
                            echo $this->image_lib->display_errors();
                        }

                        else {
                            $padVerwijderen = './uploads/'.$fotoURL;
                            unlink($padVerwijderen);
                            $pad = $upload_data["file_name"];
                            $s->padProfiel = $pad;
                            //redirect('gebruiker/login');
                        }
                    }
                }
            }

            $s->updateProfileData($nieuwPaswoord);
            $this->session->set_flashdata('data', 'Uw profiel is aangepast!');
            redirect('IMDStudent/home');
        }

        else {
        }                
    } 

    public function datumGidsen()  
    {

        $this->load->model("Dates_model", '', true);
        $this->load->model("Student_model", '', true);
        $this->load->library('session');

        $data = [
        "dates" => $this->Dates_model->get_all(),
        "opgeslagen" => $this->Dates_model->get_saved_data()
        ];

    //Checken of er gepost is

        if($this->input->server('REQUEST_METHOD') == 'POST') 
        {

            if (!empty($_POST))
            {

            $array = $this->input->post(['check_list'][0]);
            $arrayGidsen = array_filter($array);    

                if($arrayGidsen)
                {
                $this->Dates_model->dataGidsenToevoegen($arrayGidsen);
                $this->session->set_flashdata('data', 'Uw datums zijn toegevoegd!');
                redirect('IMDStudent/home');
                }
            }
            else
            {
            $this->Dates_model->verwijderAlleData();
            $this->session->set_flashdata('data', 'Je hebt geen enkele datum aangeduid, hierdoor ben je uitgeschreven voor alle mogelijke data.');
            redirect('IMDStudent/home');
            }
        }
        else
        {
        $this->load->view('IMDStudent/dataGidsen', $data);
        }

    }

    public function home()  {

    $this->load->model("Bezoek_model", '', true);
    $this->load->model("Boeking_model", '', true);
    $this->load->library('session');

        if($this->session->userdata('isStudent') != true)
        {
            $this->load->view('errors/loginerror');     
        } else { 
            $boekingen_arr = $this->Bezoek_model->get_studentBezoek();
            $this->load->view('IMDStudent/home', $boekingen_arr);
        }
    }



}


