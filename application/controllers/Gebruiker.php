<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gebruiker extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }

	public function index()
	{
		echo "gebruiker/index";
	}

	public function login()
	{	
		if($this->session->userdata('isAdmin') !== NULL) {
        	redirect('admin/home');
        } else if ($this->session->userdata('isStudent') !== NULL) {
        	redirect('IMDStudent/home');
        } else{
			$this->load->library('facebook/Facebook');
			$this->load->helper(array('form', 'url'));
		    $this->load->library('form_validation');

			$this->load->model("Gebruiker_model", '', true);
			$this->load->model('Valideer_gebruiker', '', true);

			$this->form_validation->set_rules('login_emailadres', 'E-mail', 'trim|required|valid_email', 
	            array('required' => 'Gelieve jouw emailadres in te vullen.',
	            	'valid_email' => 'Het emailadres is niet geldig.'
	            	)
	        );
			$this->form_validation->set_rules('login_paswoord', 'Paswoord', 'trim|required|min_length[6]',
				array('required' => 'Gelieve jouw wachtwoord in te vullen.',
					'min_length' => 'Het wachtwoord moet minstens 6 tekens bevatten.'
	            	)

				);
			
				if ($this->form_validation->run() == FALSE)
		        {
					$this->load->view('gebruiker/login');
		        } else
		        {
		        	$Email = $this->input->post('login_emailadres');
					$Paswoord = $this->input->post('login_paswoord');

					//$data = array('emailadres' => $Email, 'paswoord' => $Paswoord);

					if($this->Valideer_gebruiker->validate_user($Email, $Paswoord) == "admin") 
					{
	          			redirect('admin/home');
	      			} else if($this->Valideer_gebruiker->validate_user($Email, $Paswoord) == "student")
	      			{
	      				redirect('IMDStudent/home');
	      			} else {

						$data = [
	      					"errorValidatie" => "We kunnen jouw e-mailadres en/of paswoord niet herkennen."
	      				];

	         			$this->load->view('gebruiker/login', $data);
	      			}
	      		}
	        
	    }
	}

	public function logout()
	{	

		$this->load->library('session');
		$this->load->library('facebook/Facebook');

		if ($this->facebook->session) {
			$this->session->sess_destroy();
			redirect('gebruiker/login');
		}
		else
		{
			if($this->session->userdata('isStudent') == true)
	        {
	            $this->session->sess_destroy();
	            redirect('gebruiker/login');
	        }
	        else 
	        {
	            if($this->session->userdata('isAdmin') == true)
	        	{
	        		$this->session->sess_destroy();
	        		redirect('gebruiker/login');
	        	} 
			}
		}
	}

}
