<?php

class Bezoeker extends CI_Controller {


	public function index()
	{
		$this->load->view('bezoeker/home');
	}

	public function home()
	{
		$this->load->library('session');
		$this->load->library('facebook/Facebook');
		$this->load->library('form_validation');
		$this->load->model("Bezoeker_model", '', true);
		$this->load->model("Bezoek_model", '', true);

		if ($this->facebook->session) {
				$br = new Bezoeker_model();
				$br->voornaam = $this->facebook->get_user()['first_name'];
				$br->achternaam = $this->facebook->get_user()['last_name'];
				$br->email = $this->facebook->get_user()['email'];
				$br->fbuserid = $this->facebook->get_user()['id'];
				$br->save($br, $this->facebook->get_user()['id']);
		}

				
		if ($this->facebook->session) {
			$boekingen_arr = $this->Bezoek_model->get_bezoekerBezoek();
    		$this->load->view('bezoeker/home', $boekingen_arr);
    		// We geven de data hier mee naar de view, zodat die daar gekend is
		}
		else
		{
			$this->load->view('gebruiker/login');
		}
	}

	public function bezoekAanmaken()
	{
		$this->load->library('session');
		$this->load->library('facebook/Facebook');

		$this->load->model("Student_model", '', true);
		$this->load->model("Bezoeker_model", '', true);
		$this->load->model("Dates_model", '', true);
		$data = [
			"hello"  => "world",
			"dates" => $this->Dates_model->get_all(),
			"profielen" => $this->Student_model->get_all(),
			"bezoeker" => $this->Bezoeker_model->get_allBezoekerId()
		];


		$this->load->model("Bezoek_model", '', true);
		$bezoeken = [
			"hello"  => "world",
			"bezoeken" => $this->Bezoek_model->get_all()
		];
		if($this->input->server('REQUEST_METHOD') == 'POST')
		{
			//input uit form lezen
			$datum = $this->input->post('Datum');
			$uur = $this->input->post('Uur');
			$gids = $this->input->post('student');
			$bezoeker = $this->input->post('bezoekerId');
			
	        //$bezoeker = $this->db->select('id')->where('email','yoshimitsu_blauw@hotmail.com')->get("bezoeker"); 
			
			//maak dan een neuw object Bezoek_model()
			$b = new Bezoek_model();
			$b->datum = $datum;
			$b->uur = $uur;
			$b->IMDStudentId = $gids;
			$b->BezoekerId = $bezoeker;

			//save() via het model naar de db
			$this->Bezoek_model->save($b);
		}
		if ($this->facebook->session) {
			$this->load->view('bezoeker/bezoekAanmaken',$data, $bezoeken);
		}
		else
		{
			$this->load->view('gebruiker/login');
		}
		
	}

	public function feedbackBezoek()
	{
		$this->load->model("Feedback_model", '', true);
		$this->load->library('session');
		$this->load->library('facebook/Facebook');
		if ($this->facebook->session) {

			if($this->input->server('REQUEST_METHOD') == 'POST') 
			{

				$error = false;
				$required = array('sfeer', 'contact', 'indruk', 'aanraden', 'tekst-verbeteringen', 'tekst-positief');
				
				foreach($required as $field)
				{
					if (empty($_POST[$field])) {
						$error = true;
					}
				}

				if ($error) {
					 $data = [
	                  "errorValidatie" => "Gelieve all vragen te beantwoorden."
	                    ];

					$this->load->view('bezoeker/feedback',$data);
				} else {

				$sfeer = $this->input->post('sfeer');
				$contact = $this->input->post('contact');
				$indruk = $this->input->post('indruk');
				$aanraden = $this->input->post('aanraden');
				if ($aanraden == "ja") {
				$aanraden = True;
				}
				else {
				$aanraden = False;	
				}
				$verbeteringen = $this->input->post('tekst-verbeteringen');
				$positief = $this->input->post('tekst-positief');
				$toestemming = $this->input->post(['toestemming'][0]);
				if ($toestemming == "1") {
				$aanraden = True;
				}
				else {
				$aanraden = False;	
				}

				$user = $this->facebook->get_user();
				$bezoekerId = $this->Feedback_model->getBezoekerId($user['id']);
				$data = [
				 "bezoekerId" => $bezoekerId[0]['id'],
	             "sfeer" => $sfeer,
	             "contact" => $contact,
	             "indruk" => $indruk,
	             "aanraden" => $aanraden,
	             "verbeteringen" => $verbeteringen,
	             "positief" => $positief,
	             "toestemming" => $toestemming
	                    ];
	            $this->Feedback_model->feedbackOpslaan($data);
	            $this->session->set_flashdata('success', 'Je feedback is goed ontvangen.');
                redirect('bezoeker/home');
				}
			} else 
			{
				$this->load->view('bezoeker/feedback');
			}
		
		} else 
		{
			$this->load->view('gebruiker/login');
		}

	}

	public function gidsLijst()
    {
    	
    		$this->load->library('session');
    		$this->load->library('facebook/Facebook');
        	if ($this->facebook->session) 
        	{
        		$this->load->model('Profiel_model', '', true);
                $profielen = [
					"profielen" => $this->Profiel_model->get_all()
				];
				$this->load->view('bezoeker/gidslijst', $profielen);
        	} 
        	else 
        	{
				$this->load->view('gebruiker/login');
		    }
	}

	public function gidsProfiel()
    {
    	
    		$this->load->library('session');
    		$this->load->library('facebook/Facebook');
    		$this->load->model('Profiel_model', '', true);
    		$this->load->model('Student_model', '', true);
    		$this->load->model('Dates_model', '', true);
    		$this->load->model('Bezoeker_model', '', true);
    		$this->load->model("Bezoek_model", '', true);

			if ($this->facebook->session) {
				$profiel = [
					"loggedin" => 'bezoeker',
					"profiel" => $this->Profiel_model->get_one(),
					"dates" => $this->Student_model->get_oneDataGids(),
					"datums" => $this->Dates_model->get_all(),
					"bezoeker" => $this->Bezoeker_model->get_allBezoekerId()
				];
				$this->load->view('bezoeker/gidsprofiel', $profiel);
			}
			else
			{
				if($this->session->userdata('isStudent') == true)
		        {
				$profiel = [
					"loggedin" => 'student',
					"profiel" => $this->Profiel_model->get_one()
				];
				$this->load->view('bezoeker/gidsprofiel', $profiel);
		        }
		        else 
		        {
		            if($this->session->userdata('isAdmin') == true)
		        	{
						$profiel = [
							"loggedin" => 'admin',
							"profiel" => $this->Profiel_model->get_one()
						];
						$this->load->view('bezoeker/gidsprofiel', $profiel);
		        	} 
		        	else 
		        	{
		        		$profiel = [
							"loggedin" => 'nee',
							"profiel" => $this->Profiel_model->get_one(),
							"dates" => $this->Student_model->get_oneDataGids(),
							"datums" => $this->Dates_model->get_all()
						];
						$this->load->view('bezoeker/gidsprofiel', $profiel);
			        }
				}
			}

			if($this->input->server('REQUEST_METHOD') == 'POST')
			{
				//input uit form lezen
				$datum = $this->input->post('Datum');
				$uur = $this->input->post('Uur');
				$gids = $this->input->post('student');
				$bezoeker = $this->input->post('bezoekerId');
				
		        //$bezoeker = $this->db->select('id')->where('email','yoshimitsu_blauw@hotmail.com')->get("bezoeker"); 
				
				//maak dan een neuw object Bezoek_model()
				$b = new Bezoek_model();
				$b->datum = $datum;
				$b->uur = $uur;
				$b->IMDStudentId = $gids;
				$b->BezoekerId = $bezoeker;

				//save() via het model naar de db
				$this->Bezoek_model->save($b);
			}
	}


	public function gidsBeoordelen($id)
	{
		$this->load->library('session');
		$this->load->library('facebook/Facebook');
		if ($this->facebook->session) 
		{
			$this->load->model('Gidsbeoordeling_model', '', true);
			$checkBezoekId = $this->Gidsbeoordeling_model->check_BezoekId($id);
			if ($checkBezoekId)
			{
				if($this->input->server('REQUEST_METHOD') == 'POST') 
				{
					$error = false;
					$required = array('vakken','campus','sociaal','communicatie','quote');
					foreach($required as $field)
					{
						if (empty($_POST[$field])) {
							$error = true;
						}
					}
					if ($error) {
						$data = [
						"errorValidatie" => "Gelieve alle vragen te beantwoorden."
						];
						$this->load->view('bezoeker/gidsBeoordeling',$data);
					} else {
						$vakken = $this->input->post('vakken');
						$campus = $this->input->post('campus');
						$sociaal = $this->input->post('sociaal');
						$communicatie = $this->input->post('communicatie');
						$quote = $this->input->post('quote');
						$rating = ($vakken+$campus+$sociaal+$communicatie)/4;
						$dataStudent = $this->Gidsbeoordeling_model->get_studentId($id);
						$studentID = $dataStudent[0]['IMDStudentID'];
						$data = [
						"BezoekId" => $id,
						"IMDStudentId" => $studentID,
						"rating" => $rating,
						"quote" => $quote
						];
						$this->Gidsbeoordeling_model->ratingOpslaan($data);
						redirect('bezoeker/home');
					}
				}

				else {
					$this->load->view('bezoeker/gidsBeoordeling');
				}
			}
			else 
			{
				redirect('bezoeker/home');
			}
		}
		else 
		{
			$this->load->view('gebruiker/login');
		}
	}

	public function Popular()
    {
    	
    		$this->load->library('session');
    		$this->load->library('facebook/Facebook');
        	if ($this->facebook->session) 
        	{
        		$this->load->model('Rankinggids_model', '', true);
                $data = $this->Rankinggids_model->get_ratings();
                $rankings = [
                        "rankings" => $data
                    ];
				$this->load->view('bezoeker/ratingGidsen', $rankings);
        	} 
        	else 
        	{
				$this->load->view('gebruiker/login');
		    }
		    
	}

}


