<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chat extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('facebook/Facebook');
        $this->load->model('Chat_model');
        $this->load->model('Student_model');
    }
		
	public function bericht($bezoeker, $student)
	{
		if(!isset( $this->session->userdata['id']) && !isset($this->facebook->get_user()['id']) )
		{
			$this->load->view('errors/loginerror');				
		} else 
		{
			if ( isset($this->facebook->get_user()['id']) && $this->facebook->get_user()['id'] == $bezoeker ) {
				$me =  $bezoeker;
				$you = $student;
				$data['loggedin'] = 'bezoeker';
			} else if ( isset($this->session->userdata['id']) && $this->session->userdata['id'] == $student )  {
				$me =  $student;
				$you = $bezoeker;
				$data['loggedin'] = 'student';
	    	} else {
	    		redirect('home/index');
	    	}
			
			$chat = new Chat_model();

			if ( empty($chat->CheckForBookings($bezoeker, $student)) ) 
			{
				redirect('chat/index');
			}

			if ( empty($chat->CheckChat($bezoeker, $student)) )
			{
				$chat->MaakChat($bezoeker, $student);
			} 
				$chatArray = $chat->GetChatId($bezoeker, $student);
				$chatId = $chatArray[0]['chatId'];

				if( !empty($this->input->post()) && !empty(trim($this->input->post('text'))) )
				{						
					$text = html_escape($this->input->post('text'));
					$chat->setText( $text );
					$chat->setZender($me);
					$chat->setOntvanger($you);
					$chat->CreateBericht($chatId);
				}
				$data['studentInfo'] = $this->Student_model->OneStudentData($student);
				$alleBerichten = $chat->GetAllBerichten($chatId);
				$data['chatId'] = $chatId;
				$data['alleBerichten'] = $alleBerichten;
				$data['student'] = $student;
				$data['bezoeker'] = $bezoeker;
				$data['me'] = $me;
				$data['you'] = $you;
				$this->load->view('chat/bericht.php', $data);

		}
	}


	public function verwerkBerichtAjax($me, $you, $bezoeker, $student, $chatId)
	{
		if ( isset($this->facebook->get_user()['id']) && $this->facebook->get_user()['id'] == $bezoeker ) {
				$me =  $bezoeker;
				$you = $student;
			} else if ( isset($this->session->userdata['id']) && $this->session->userdata['id'] == $student )  {
				$me =  $student;
				$you = $bezoeker;
				$data['loggedin'] = 'student';
	    	} else {
	    		redirect('home/index');
	    	}
		
		if ($this->input->is_ajax_request()) {
			$bericht = html_escape( $this->input->post('bericht') );
			$ichat = new Chat_model();
			$ichat->setText($bericht);
			$ichat->setZender($me);
			$ichat->setOntvanger($you);
			$ichat->CreateBericht($chatId);
			$this->output->set_content_type('application/json')->set_output(json_encode( array('bericht' =>  $bericht, 'status' => 'success' ) ) );
		}

	}

	public function updateBerichtenAjax($me, $you, $bezoeker, $student, $chatId)
	{
		if ( isset($this->facebook->get_user()['id']) && $this->facebook->get_user()['id'] == $bezoeker ) {
				$me =  $bezoeker;
				$you = $student;
			} else if ( isset($this->session->userdata['id']) && $this->session->userdata['id'] == $student )  {
				$me =  $student;
				$you = $bezoeker;
				$data['loggedin'] = 'student';
	    	} else {
	    		redirect('home/index');
	    	}
		
		if ($this->input->is_ajax_request()) {
			$ichat = new Chat_model();
			$update = $ichat->Update($you, $chatId);
			if (!empty($update)) {
				$this->output->set_content_type('application/json')->set_output(json_encode( array('bericht' =>  $update, 'status' => 'success' ) ) );
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode( array('status' => 'false' ) ) );
			}
			
		}

	}

	public function MailAlsNieuwBericht($cronName)
	{
		if (!$cronName == "MailAlsNieuwBericht") {
			redirect("home");
		}

			var_dump($this->Chat_model->NieuwBericht());
			if ($this->Chat_model->NieuwBericht()) {
				$mails = $this->Chat_model->NieuwBericht();
				foreach ($mails as $key => $value) {
					echo "<br>".$mails[$key]['email'];
					echo "<br>".$mails[$key]['ontvangerId'];
					echo "<br>".$mails[$key]['zenderId'];
					
							$ci = get_instance();
							$ci->load->library('email');
							$config['protocol'] = "smtp";
							$config['smtp_host'] = "smtp.mandrillapp.com"; //Wachtwoord Mandrill = Rentastudent!
							$config['smtp_port'] = "587";
							$config['smtp_user'] = "thomasmorerentastudent@gmail.com"; 
							$config['smtp_pass'] = "exZVeV8oBPyqZ-DCr0XCTQ"; //API KEY MANDRILL
							$config['charset'] = "utf-8";
							$config['mailtype'] = "html";
							$config['newline'] = "\r\n";
							$ci->email->initialize($config);
							$ci->email->from('thomasmorerentastudent@gmail.com', 'RentAStudent');
							$ci->email->to( $mails[$key]['email'] );
							$ci->email->reply_to('thomasmorerentastudent@gmail.com', 'RentAStudent');
							$ci->email->subject('Je hebt een bericht ontvangen.');
							$ci->email->message('Beste gids, je hebt een bericht ontvangen van een bezoeker:<br>'.'<a href="'. site_url('chat/bericht/' . $mails[$key]['zenderId'] . '/' . $mails[$key]['ontvangerId']).'">'.site_url('chat/bericht/' . $mails[$key]['zenderId'] . '/' . $mails[$key]['ontvangerId']).'</a>' );
							$ci->email->send();
					
				}
				echo "<br> mails verstuurd";
			} else {
				echo "Geen mails verstuurd";
			}
			

	}

}
			

			
	