<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->library('session');
		$this->load->library('facebook/Facebook');
		$this->load->model('Home_model', '', true);
		$this->load->model('Profiel_model', '', true);
		$quotes = $this->Home_model->getQuotes();
		$images = $this->Profiel_model->get_all();

		if ($this->facebook->session) {
			$loggedin = [
						"loggedin" => 'bezoeker',
						"quotes" => $quotes,
						"images" => $images
					];
			$this->load->view('home', $loggedin);
		}
		else
		{
			if($this->session->userdata('isStudent') == true)
	        {
	            $loggedin = [
						"loggedin" => 'student',
						"quotes" => $quotes,
						"images" => $images
					];  
	            $this->load->view('home', $loggedin); 
	        }
	        else 
	        {
	            if($this->session->userdata('isAdmin') == true)
	        	{
	        		$loggedin = [
						"loggedin" => 'admin',
						"quotes" => $quotes,
						"images" => $images
					];
	        		$this->load->view('home', $loggedin);
	        	} 
	        	else 
	        	{
	        		$loggedin = [
						"loggedin" => 'nee',
						"quotes" => $quotes,
						"images" => $images
					];
	        		$this->load->view('home', $loggedin);
		        }
			}
		}
	}
}