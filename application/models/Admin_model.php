<?php
class Admin_model extends CI_model
{

	public function checkDate() {
		
		//$query = "SELECT * FROM bezoeken WHERE datum < CURRENT_DATE() AND Notified = 0";
		//$query = "SELECT IMDStudentId, SUM(rating) AS 'Totaal', COUNT(rating) AS 'rondleidingen', ROUND((SUM(rating)/COUNT(rating)),2) AS 'Gemiddelde', profielen.voornaam, profielen.achternaam FROM ratinggidsen INNER JOIN profielen ON ratinggidsen.IMDStudentId=profielen.id GROUP BY IMDStudentId ORDER BY Gemiddelde DESC";
		$query = "SELECT bezoeken.id, bezoeker.voornaam, bezoeker.email FROM bezoeken INNER JOIN bezoeker ON bezoeken.BezoekerId = bezoeker.id WHERE datum < CURRENT_DATE() AND Notified = 0";
		$data = $this->db->query($query);
		$dataArray = $data->result_array();
		return $dataArray;
	}

	public function updateNotified($p_id) {

		$data = array(
			'Notified' => "1"
			);
		$this->db->where('id', $p_id);
		$this->db->update('bezoeken', $data);
	}

}