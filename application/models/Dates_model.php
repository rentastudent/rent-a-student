<?php
	class Dates_model extends CI_Model
	{

		public $datum;

		public function save($d)
		{
			$this->db->insert('data', $d);
		}

		public function get_all()
		{
			//query builder
			$dates = $this->db->order_by('datum', 'ASC')->get("data");
			return $dates->result_array();
		}
		public function getOne($p_id)
		{
			$statement = $this->db->get_where('data', array('id' => $p_id));
			return $statement;
		}

		public function delete($p_id)
		{
			$result= $this->db->delete('data', array('id' => $p_id)); 
			return $result;
		}

		public function dataGidsenToevoegen($dataGidsen)
        {
            //Session email gaan vergelijken met de ID die daar aan toegekend is in de tabel profielen.

            $arr_id = $this->session->userdata('id');
                //Voor elke waarde die in de array zat = een geselecteerde checkbox, wordt de DatumID en de ID van gebruiker, die we in de vorige
                // stap gefilterd hebben, in een array gestoken die we erna in een nieuwe tabel schrijven.
            
            $this->db->select('DatumId');
            $this->db->from('datagidsen');
            $this->db->where('IMDStudentID', $arr_id);
            $sql = $this->db->get();
            $EmptyTestArray = $sql->num_rows();
            $array = $sql->result_array();

            foreach ($dataGidsen as $key => $d)
            {
            	if ($EmptyTestArray != 0) 
            	{
            		foreach ($array as $kee => $value) {
            			if ($d != $value['DatumId'])
						{ //De id uit de checkbox komt niet overeen met de tabel. Hij saved de datum naar de tabel.
							$data = array(
								'DatumId' => $d,
								'IMDStudentID' => $arr_id
								);    
							$this->db->insert('datagidsen', $data);
						}
					}
				}
				else {
					$data = array(
						'DatumId' => $d,
						'IMDStudentID' => $arr_id
						);    
					$this->db->insert('datagidsen', $data);
				}	
			}
		}

		public function get_saved_data()
		{

			$this->db->select('id');
            $this->db->from('profielen');
            $this->db->where('email', $this->session->userdata('emailadres'));
            $sql = $this->db->get();
            $array = $sql->result_array();
            $id = $array[0]['id'];

            $this->db->select('DatumId');
            $this->db->from('datagidsen');
            $this->db->where('IMDStudentID', $id);
            $sql = $this->db->get();
            $EmptyTestArray = $sql->num_rows();
            $array = $sql->result_array();

			$this->db->select('*');
            $this->db->from('datagidsen');
            $this->db->where('IMDStudentID', $id);
            $sql = $this->db->get();
            $arr = $sql->result_array();
            return $arr; 
		}

		public function verwijderAlleData ()
		{
			$this->db->select('id');
            $this->db->from('profielen');
            $this->db->where('email', $this->session->userdata('emailadres'));
            $sql = $this->db->get();
            $array = $sql->result_array();
            $id = $array[0]['id'];

            $this->db->where('IMDStudentID', $id);
			$this->db->delete('datagidsen');

		}

	}
?>