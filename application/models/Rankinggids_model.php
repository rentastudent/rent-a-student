<?php
	class Rankinggids_model extends CI_Model
	{
		public function get_ratings()
		{
	        $query = "SELECT IMDStudentId, SUM(rating) AS 'Totaal', COUNT(rating) AS 'rondleidingen', ROUND((SUM(rating)/COUNT(rating)),2) AS 'Gemiddelde', profielen.voornaam, profielen.achternaam, profielen.id FROM ratinggidsen INNER JOIN profielen ON ratinggidsen.IMDStudentId=profielen.id GROUP BY IMDStudentId ORDER BY Gemiddelde DESC";
	        $data = $this->db->query($query);
	        return $data->result_array();

	    }
	}

?>