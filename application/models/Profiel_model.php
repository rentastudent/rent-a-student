<?php
	class Profiel_model extends CI_Model
	{

		public function get_all()
		{
			$profielen = $this->db->get("profielen");
			return $profielen->result_array();
		}

		public function get_one()
		{
			$profiel_id = $this->uri->segment(3, 0);
			if ($profiel_id > 2) 
			{
				$this->db->select('*');
	            $this->db->from('profielen');
	            $this->db->where('id', $profiel_id);
	            $sql = $this->db->get();
	            $profiel = $sql->result_array();
	            return $profiel;
			} 
		}

	}