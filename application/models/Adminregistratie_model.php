<?php
	class Adminregistratie_model extends CI_model
	{
		private $m_sVoornaam;
		private $m_sAchternaam;
		private $m_sEmail;
		private $m_sPaswoord;

		private $m_sVEmail;

		public function __set($p_sProperty, $p_sValue)
		{

			switch ($p_sProperty) {
				case 'Voornaam':
					$this->m_sVoornaam= $p_sValue;
					break;

				case 'Achternaam':
					$this->m_sAchternaam= $p_sValue;
					break;

				case 'Email':
					$this->m_sEmail = $p_sValue;
					break;

				case 'VerwijderEmail':
					$this->m_sVEmail = $p_sValue;
					break;

				case 'Paswoord':
					$options = [
	    			'cost' => 10
					];
					$this->m_sPaswoord = password_hash($p_sValue, PASSWORD_DEFAULT, $options);
					break;
			}
		}

		public function save()
		{
			$data = array(
	       		'voornaam' => $this->m_sVoornaam,
	       	 	'achternaam' => $this->m_sAchternaam,
	       	 	'emailadres' => $this->m_sEmail,
	       	 	'paswoord' => $this->m_sPaswoord
			);
			$this->db->insert('admin_gebruiker', $data); 
		}	

		public function deleteAdmin()
		{
			
			$this->db->select('*');
			$this->db->from('admin_gebruiker');
			$this->db->where('emailadres', $this->m_sVEmail);
			$sql = $this->db->get();

			if($sql->num_rows() == 1){
				$this->db->delete('admin_gebruiker', array('emailadres' => $this->m_sVEmail));
				return 'true';
			} else {
				return 'false';
			}


			
		}

	}