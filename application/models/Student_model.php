<?php
class Student_model extends CI_Model {

        private $m_sVoornaam;
        private $m_sAchternaam;
        private $m_sEmail;
        private $m_sPaswoord;
        private $m_sStudiejaar;
        private $m_sAfstudeerrichting;
        private $m_iTelefoonnummer;
        private $m_sWoonplaats;
        private $m_sPadProfiel;
        private $m_sMotivatie;
    
        public function save(){
      	  	
       		$data = array(
                    'voornaam' => $this->m_sVoornaam,
                    'achternaam' => $this->m_sAchternaam,
                    'email' => $this->m_sEmail,
                    'paswoord' => $this->m_sPaswoord,
                    'studiejaar' => $this->m_sStudiejaar,
                    'afstudeerrichting' => $this->m_sAfstudeerrichting,
                    'telefoonnummer' => $this->m_iTelefoonnummer,
                    'woonplaats' => $this->m_sWoonplaats,
                    'motivatie' => $this->m_sMotivatie,
                    'padProfiel' => $this->m_sPadProfiel
                );    

            $this->db->insert('profielen', $data);
        }

        public function getProfileData() {

        //$query = $this->db->query("SELECT * FROM PROFIELEN WHERE id = 1");
        //return $query->result_array();
        $this->load->library('session');

        //print_r($this->session->userdata);
        if($this->session->userdata('isStudent') == true)
        {
           
            $this->db->select('*');
            $this->db->from('profielen');
            $this->db->where('email', $this->session->userdata('emailadres'));
            $sql = $this->db->get();
            $arr = $sql->result_array();
            return $arr; 
        }

        }

        public function updateProfileData($p_sPaswoord) {

        $this->db->select('*');
        $this->db->from('profielen');
        //$this->db->where('email',"lpoignonnec@hotmail.be");
        $this->db->where('email',$this->session->userdata('emailadres'));
        $sql = $this->db->get();
        $arr = $sql->result_array();
        $arr_pass = $arr[0]['paswoord'];

        //Controleren het invulveld leeg was, zo ja, geeft hij het oude wachtwoord mee uit de array, anders de variabele.
        if (empty($p_sPaswoord)) {
        $wachtwoord =  $arr_pass;
        }

        else {
        $wachtwoord = $this->m_sPaswoord;
        }

        $data = array(
                    'voornaam' => $this->m_sVoornaam,
                    'achternaam' => $this->m_sAchternaam,
                    'email' => $this->m_sEmail,
                    'paswoord' => $wachtwoord,
                    'studiejaar' => $this->m_sStudiejaar,
                    'afstudeerrichting' => $this->m_sAfstudeerrichting,
                    'telefoonnummer' => $this->m_iTelefoonnummer,
                    'woonplaats' => $this->m_sWoonplaats,
                    'motivatie' => $this->m_sMotivatie,
                    'padProfiel' => $this->m_sPadProfiel
                );    
        
        //$this->db->where('id', 1);
        $this->db->where('email',$this->session->userdata('emailadres'));
        $this->db->update('profielen', $data);

        }

        public function controleerHuidigPaswoord($p_sPaswoord) {

        $this->db->select('*');
        $this->db->from('profielen');
        //$this->db->where('email',"lpoignonnec@hotmail.be");
        $this->db->where('email',$this->session->userdata('emailadres'));
        $sql = $this->db->get();

        if($sql->num_rows() == 1){
        $arr = $sql->result_array();
        $arr_pass = $arr[0]['paswoord'];
            if (password_verify($p_sPaswoord , $arr_pass)) {
                    return true;
                } else{
                    return false;
                }

            }
        }
        
        public function get_allDataGidsen()
        {
            //query builder
            $data = $this->db->get("datagidsen");
            return $data->result_array();
        }
        public function get_oneDataGids()
        {
            $profiel_id = $this->uri->segment(3, 0);
            $this->db->select('*');
            $this->db->from('datagidsen');
            $this->db->where('IMDStudentId', $profiel_id);
            $sql = $this->db->get();
            $datas = $sql->result_array();
            return $datas;
        }
         public function get_all()
        {
            $profielen = $this->db->get("profielen");
            return $profielen->result_array();
        }
    
        public function __set($p_sProperty, $p_vValue)
		{
			switch ($p_sProperty) {
				case 'voornaam':
					$this->m_sVoornaam = $p_vValue;
                    break;

				case 'achternaam':
					$this->m_sAchternaam = $p_vValue;
					break;

				case 'email':               
					$this->m_sEmail = $p_vValue;
					break;
                
                case 'paswoord':
					$options = [
                    'cost' => 10
                    ];
                    $this->m_sPaswoord = password_hash($p_vValue, PASSWORD_DEFAULT, $options);
                    break;

				case 'studiejaar':
					$this->m_sStudiejaar = $p_vValue;
					break;

                case 'afstudeerrichting':
                    $this->m_sAfstudeerrichting = $p_vValue;
                    break;

				case 'telefoonnummer':
					$this->m_iTelefoonnummer = $p_vValue;
					break;

                case 'woonplaats':
                    $this->m_sWoonplaats = $p_vValue;
                    break;

                case 'motivatie':
                    $this->m_sMotivatie = $p_vValue;
                    break;

                case 'padProfiel':
                    $this->m_sPadProfiel = $p_vValue;
                    break;
			}
		}

        public function OneStudentData($ids) {
           
                $this->db->select('*');
                $this->db->from('profielen');
                $this->db->where('id', $ids);
                $sql = $this->db->get();
                $arr = $sql->result_array();
                return $arr; 

        }

}