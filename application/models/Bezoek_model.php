<?php
	class Bezoek_model extends CI_Model
	{

		public $datum;
		public $uur;
		public $IMDStudentId;
		public $BezoekerId;

		public function save($b)
		{
			$this->db->insert('bezoeken', $b);
		}

		public function get_all()
		{
			//query builder
			$bezoeken = $this->db->get("bezoeken");
			return $bezoeken->result_array();
		}

		public function get_allPlusBezoeker()
		{
			$array = array(
    				"bezoeken" => $this->db->select('*')->get("bezoeken")->result_array(),
    				"bezoekers" => $this->db->select('*')->get("bezoeker")->result_array()
				);
			
			return $array;
		}

		public function get_studentId($p_id)
		{

			$this->db->select('IMDStudentID');
            $this->db->from('bezoeken');
            $this->db->where('id', $p_id);
            $sql = $this->db->get();
            $arr = $sql->result_array();
            return $arr; 
		}

		public function get_studentBezoek()
		{
			$studentID = $this->session->userdata('id');
			$this->db->select('*');
            $this->db->from('bezoeken');
            $this->db->where('IMDStudentID', $studentID);
            $sql = $this->db->get();
            $arr = $sql->result_array();


            $array = array(
    				"bezoeken" => $arr,
    				"studenten" => $this->db->select('*')->get("bezoeker")->result_array()
				);
			
			return $array;
		}

		public function get_bezoekerBezoek()
		{
			$user = $this->facebook->get_user();
			$this->db->select('id');
	    	$this->db->from('bezoeker');
	        $this->db->where('email', $user['email']);
	        $query = $this->db->get();
	        $bezoeker = $query->result_array();
	        $studentID = $bezoeker[0]['id'];

			$this->db->select('*');
            $this->db->from('bezoeken');
            $this->db->where('BezoekerId', $studentID);
            $sql = $this->db->get();
            $arr = $sql->result_array();


            $array = array(
    				"bezoeken" => $arr,
    				"studenten" => $this->db->select('*')->get("profielen")->result_array()
				);
			
			return $array;
		}

	}
?>