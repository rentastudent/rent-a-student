<?php
	class Feedback_model extends CI_Model
	{

		public function feedbackOpslaan($dataFeedback)
        {

        $data = $dataFeedback;
        $this->db->insert('feedbackbezoeker', $data);

        }

        public function getBezoekerId($data)
        {
			$this->db->select('id');
	    	$this->db->from('bezoeker');
	        $this->db->where('fbuserid', $data);
	        $sql = $this->db->get();
	        $bezoekerId = $sql ->result_array();
	        return $bezoekerId;
        }

    	public function get_all()
		{
			//query builder
			$query = "SELECT bezoeker.voornaam, bezoeker.achternaam, bezoeker.email, feedbackbezoeker.* FROM feedbackbezoeker INNER JOIN bezoeker ON feedbackbezoeker.bezoekerId = bezoeker.id";
			$feedback = $this->db->query($query);
			return $feedback->result_array();
		}

	}