<?php
	class Chat_model extends CI_Model
	{
		private $m_sText;
		private $m_sZender;
		private $m_sOntvanger;

		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		public function setText($p_sValue)
		{
			$this->m_sText = $p_sValue;
		}

		public function setZender($p_sValue)
		{
			$this->m_sZender = $p_sValue;
		}

		public function setOntvanger($p_sValue)
		{
			$this->m_sOntvanger = $p_sValue;
		}

		public function getText()
		{
			return $this->m_sText;
		}

		public function getZender()
		{
			return $this->m_sZender;
		}

		public function getOntvanger()
		{
			return $this->m_sOntvanger;
		}

		public function CreateBericht($chatId)
		{
			$data = [
					"chatId" => $chatId,
					"bericht" => $this->getText(),
					"zenderId" => $this->getZender(),
			        "ontvangerId" => $this->getOntvanger(),
			        "uur" => time(),
			        "datum" => date("m-d-y")
					];
			$this->db->insert('berichten', $data);
		}

		public function GetAllBerichten($chatId)
		{
			$this->db->select('*');
			$this->db->where('chatId', $chatId);
			$query = $this->db->get('berichten')->result_array();
			return $query;
		}

		public function CheckChat($bezoeker, $student)
		{
			$this->db->select('*');
			$this->db->where('bezoekerfbId', $bezoeker);
			$this->db->where('studentId', $student);
			$query = $this->db->get('chat')->result_array();
		    return $query;
		}

		public function MaakChat($bezoeker, $student)
		{
			$data = [
					"bezoekerfbId" => $bezoeker,
			        "studentId" => $student
					];
			$this->db->insert('chat', $data);
		}

		public function GetAllChat($p_iId)
		{
			$this->db->select('studentId');
			$this->db->where('bezoekerfbId', $p_iId);
			$query = $this->db->get('chat')->result_array();
		    return $query;
		}

		public function GetChatId($bezoeker, $student)
		{
			$this->db->select('chatId');
			$this->db->where('bezoekerfbId', $bezoeker);
			$this->db->where('studentId', $student);
			$query = $this->db->get('chat')->result_array();
		    return $query;
		}

		public function CheckForBookings($bezoeker, $student)
		{
			try {

				$this->db->select('*');
				$this->db->where('fbuserid', $bezoeker);
				$queryB = $this->db->get('bezoeker')->result_array();
				if ( count($queryB) ) {
					$bezoekerId = $queryB[0]['id'];
					$this->db->select('*');
					$this->db->where('BezoekerId', $bezoekerId);
					$this->db->where('IMDStudentId', $student);
					$query = $this->db->get('bezoeken')->result_array();
			    	return $query;
				} else {
					return false;
				}
			} catch (Exception $e) {
				
			}
		}

		public function GetStudentEmail($student)
		{
				$this->db->select('email');
				$this->db->where('id', $student);
				$queryC = $this->db->get('profielen')->result_array();
				return $queryC;
		}

		public function Update($you, $chatId)
		{
			$this->db->select('bericht');
			$this->db->where('zenderId', $you);
			$this->db->where('chatId', $chatId);
			$this->db->where('uur >', time()-6);
			$this->db->order_by("uur","desc");
			$query = $this->db->get('berichten')->result_array();
			return $query;
		}

		public function NieuwBericht()
		{
			$this->db->select('*');
			$queryA = $this->db->get('chat')->result_array();

			foreach ($queryA as $key => $value) {
				$this->db->select('*');
				$this->db->where('datum', date("m-d-y"));
				$this->db->where('uur >', time()-120);
				$this->db->distinct();
				$this->db->where('ontvangerId', $value['studentId']);
				$queryB = $this->db->get('berichten')->result_array();
				if ( !empty($queryB) ) {
					$this->db->select('email');
					$this->db->where('id', $queryB[$key]['ontvangerId']);
					$queryB[$key]['email'] = $this->db->get('profielen')->result_array()[0]['email'];
				}
				
			}
			if ( !empty($queryB) ) {
				return $queryB;
			} else {
				return false;
			}

		}


	}
