<?php
	class Bezoeker_model extends CI_Model
	{

		public $voornaam;
		public $achternaam;
		public $email;
		public $fbuserid;

		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		public function feedbackOpslaan($dataFeedback)
        {

        $data = $dataFeedback;
        $this->db->insert('feedbackbezoeker', $data);

        }

    	public function get_all()
		{
			//query builder
			$feedback = $this->db->get("feedbackbezoeker");
			return $feedback->result_array();
		}

		public function save($br, $fb)
		{	
			$this->db->where('fbuserid', $this->fbuserid);
			$this->db->from('bezoeker');
			if ($this->db->count_all_results() == 0) {
				$this->db->insert('bezoeker', $br);
			}
		}

		public function get_allBezoeker()
		{
			//query builder
			$bezoeker = $this->db->get("bezoeker");
			return $bezoeker->result_array();
		}

		public function get_allBezoekerId()
		{
			$this->db->select('*');
	    	$this->db->from('bezoeker');
	        $user = $this->facebook->get_user();
	        $this->db->where('email', $user['email']);
	        $bezoekerId = $this->db->get();
	        return $bezoekerId->result_array();
	    }

	}