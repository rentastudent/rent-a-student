<?php
class Gidsbeoordeling_model extends CI_Model
{
	public function get_studentId($p_id)
	{

		$this->db->select('IMDStudentID');
		$this->db->from('bezoeken');
		$this->db->where('id', $p_id);
		$sql = $this->db->get();
		$arr = $sql->result_array();
		return $arr; 
	}
	public function ratingOpslaan($b)
	{
		$this->db->insert('ratinggidsen', $b);
	}
	public function check_BezoekId($p_id)
	{

		$this->db->select('*');
		$this->db->from('ratinggidsen');
		$this->db->where('BezoekId', $p_id);
		$sql = $this->db->get();
		//$arr = $sql->result_array();
		$arr = $sql->num_rows();
		 //True betekent dat hij geen row gevonden heeft en dat de BezoekID nog niet bestaat in de tabel ratinggidsen.
		$check = true;
		if ($arr>0) {
		$check = false;
		}
		return $check;
	}

}
?>