<?php
	class Valideer_gebruiker extends CI_model
	{
		private $details = array();

		function __construct()
		{
    		parent::__construct();
		}

		function validate_user($p_sEmail, $p_sPaswoord) {
			//$query = $this->db->get('admin_gebruiker');  // Produces: SELECT * FROM mytable
			$this->db->select('*');
			$this->db->from('admin_gebruiker');
			$this->db->where('emailadres', $p_sEmail);
			$sql = $this->db->get();

			if($sql->num_rows() == 1){
				$arr = $sql->result_array();
				$arr_pass = $arr[0]['paswoord'];
				if (password_verify($p_sPaswoord , $arr_pass)) {
					$this->details = $arr[0];
					//echo $arr[0]['voornaam'];
					$this->set_adminsession();
					return "admin";
				} else{
						return false;
				}			
			} else if($sql->num_rows() != 1){
				$this->db->select('*');
				$this->db->from('profielen');
				$this->db->where('email', $p_sEmail);
				$sql = $this->db->get();

				if($sql->num_rows() == 1){
				$arr = $sql->result_array();
				$arr_pass = $arr[0]['paswoord'];
					if (password_verify($p_sPaswoord , $arr_pass)) {
						$this->details = $arr[0];
						//echo $arr[0]['voornaam'];
						$this->set_studentsession();
						return "student";
					} else{
						return false;
					}
				}
			} else {
				return false;
			}
		}

		function set_adminsession() {
			
			$this->session;
		    $this->session->set_userdata( array(
		            'id'=> $this->details['id'],
		            'naam'=> $this->details['voornaam'] . ' ' . $this->details['achternaam'],
		            'email'=>$this->details['emailadres'],
		            'isAdmin'=> true,
		            'isLoggedIn'=>true
		        )
		    );
		}

		function set_studentsession() {
			//this->load->library('session');
			$this->session;
		    $this->session->set_userdata( array(
		            'id'=> $this->details['id'],
		            'naam'=> $this->details['voornaam'] . ' ' . $this->details['achternaam'],
		            'emailadres'=> $this->details['email'],
		            'isStudent'=> true,
		            'isLoggedIn'=>true
		        )
		    );
		}
	}