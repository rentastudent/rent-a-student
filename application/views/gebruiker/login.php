<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Log in | Rent-A-Student</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="login_body">
	
	<?php $this->load->view('gebruiker/nav.inc.php'); ?>

	<?php 
	if($this->session->flashdata('success'))
	{ ?>
	<div class="alert alert-success">
	    <strong>Gelukt!</strong> <?php echo $this->session->flashdata('success'); ?>
	</div>
	<?php } ?>

	<div class="login_container">
		<h2>Inloggen</h2>

		<div class="login_link1">
			<a href="#" class="loginbtn_student">Ik ben een IMD-Student</a>
			<div id="login_Student">

				<form action="" method="post">
					<div class="form-group">
						<?php 
						if (isset($errorValidatie)) {
							echo '<div class="login_error">'. $errorValidatie .'</div>';
						}
						?>	
						<label for="login_emailadres">E-mailadres</label>
						<input id="login_emailadres" name="login_emailadres" class="form-control" value="<?php echo set_value('login_emailadres'); ?>" type="text">
						<?php 
						echo form_error("login_emailadres",'<div class="login_error">', '</div>');
						?>	
						<label for="login_paswoord">Paswoord</label>
						<input id="login_paswoord" name="login_paswoord" class="form-control" value="<?php echo set_value('login_paswoord'); ?>" type="password">
						<?php 
						echo form_error("login_paswoord",'<div class="login_error">', '</div>');
						?>	
						<button class="btn btn-default" type="submit">Meld aan</button>
					</div>
				</form>

				<a href="<?php echo site_url('IMDStudent/registratie') ?>">Registreer</a>
			</div>
		</div>

		<a href="#" class="loginbtn_bezoeker">Ik ben een Bezoeker</a>
		<div id="login_Bezoeker">
			<?php
				$login_url = $this->facebook->login_url();
				echo '<a class="btn btn-primary" href="' . $login_url . '" role="button">Log in met Facebook</a>';
			?>
		</div>

	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="<?php echo asset_url();?>js/script.js"></script>

</body>
</html>