<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Niet bevoegd</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="loginerror_body">

	<?php $this->load->view('nav.inc.php'); ?>

	<div class="login_container">
		<h2>U bent niet bevoegd om deze pagina te bekijken.</h2>
		<a href="http://localhost/Rent-A-Student/index.php/gebruiker/login">Log in.</a>
	</div>
</body>
</html>