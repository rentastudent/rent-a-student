<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rent-A-Student</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="front_body">
		<?php $this->load->view('instaModal.inc.php'); ?>

		<?php if($loggedin == 'nee'){ 
			$this->load->view('nav.inc.php');
		} 
		else if($loggedin == 'student')
		{ 
			$this->load->view('IMDStudent/nav.inc.php'); 
		} 
		else if($loggedin == 'admin')
		{ 
			$this->load->view('admin/nav.inc.php');
		} 
		else if($loggedin == 'bezoeker')
		{ 
			$this->load->view('bezoeker/nav.inc.php'); 
		} 
		?>

	<section class="front_img">
		<div class="image_gallery">
		<?php  
			foreach (array_slice($images, 0, 6) as $i) : ?>
			<?php echo "<a href='" . site_url('bezoeker/gidsprofiel') . "/" . $i['id'] ."'><img class='home_images' src='" . base_url() . "uploads/" . $i['padProfiel'] . "''alt='Profielfoto'></img></a>" ?>
		<?php endforeach; ?> 
		</div>
		<hr>
	</section>

	<section class="front_uitleg">
		<article class="uitleg_imd">
			<h3>IMD</h3>
			<p>In de richting Interactive Multimedia Design word je opgeleid tot webdesigner/webdeveloper. Als je een passie hebt voor het web en design, dan ben je hier aan het juiste adres.</p>
		</article>

		<article class="uitleg_ras">
			<h3>Rent-A-Student</h3>
			<p>Met Rent-A-Student kan je je persoonlijke gids (een IMD-student) reserveren die je zal rondleiden op de infodag. Kies een datum en een student en dan kan je de opleiding beter leren kennen.</p>
		</article>
	</section>

	<section class="front_social">
		<article class="social_quote">
			<p>Share onze site met je vrienden:</p>
			<a href="http://www.facebook.com/sharer.php?u=<?php echo site_url()?>" onclick="windowpop(this.href, 545, 433)"><img src="<?php echo asset_url();?>images/fb_logo.png" alt="Share Facebook"></a>
			<a href="https://twitter.com/home?status=Awesome%20application%20for%20%23WeAreIMD%20%23infodag%20%23RAS%0A%0A<?php echo site_url() ?>" onclick="windowpop(this.href, 545, 433)"><img src="<?php echo asset_url();?>images/twitter_logo.png" alt="Share Twitter"></a>
			<a href="https://plus.google.com/share?url=<?php echo site_url()?>" onclick="windowpop(this.href, 545, 433)"><img src="<?php echo asset_url();?>images/google_logo.png" alt="Share Google+"></a>
		</article>
	</section>
	
	<?php 
	shuffle($quotes); // Random shuffelen van de array zodat de bezoeker niet altijd dezelfde quotes ziet.
	$i = 1;
	//Array_slice om het aantal loops van de foreach lus te beperken tot 3.
	foreach (array_slice($quotes, 0, 3) as $q) { ?>
	<section class="">
		<article class="bubble">
			<p><?php echo $q['quote']?></p>
		</article>
		<span class="naamQuote"><?php echo $q['voornaam']?></span>
	</section>
	<?php $i++;}?>
	
	<script>
		function windowpop(url, width, height) {
			event.preventDefault();
		    var leftPosition, topPosition;
		    //Allow for borders.
		    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
		    //Allow for title and status bars.
		    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
		    //Open the window.
		    window.open(url, "Window2", "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
		}
	</script>
</body>
</html>