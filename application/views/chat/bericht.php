<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chat</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body class="chat_berichten">
	<?php if ($loggedin == 'bezoeker') {
		$this->load->view('bezoeker/nav.inc.php');
	?>
		<section class="chat_box">
			<?php
				foreach ($alleBerichten as $key => $eenBericht) { 
					if ($eenBericht['zenderId'] == $me) { ?>
							<article class="chat_message_me chat_message"><img src="http://graph.facebook.com/<?php echo $bezoeker; ?>/picture?type=square" alt=""><p><?php echo $eenBericht['bericht']; ?></p></article>
					<?php } else if ($eenBericht['zenderId'] == $you) { ?>
					 <article class="chat_message_you chat_message"><img src="<?php echo base_url() . 'uploads/'. $studentInfo[0]['padProfiel']; ?>" alt=""><p><?php echo $eenBericht['bericht']; ?></p></article> 
					<?php 
					}		
				} 
			?>
		</section>
	<?php
	} else if ($loggedin == 'student'){
		$this->load->view('imdstudent/nav.inc.php');

		?>
	<section class="chat_box">
		<?php
			foreach ($alleBerichten as $key => $eenBericht) { 
				if ($eenBericht['zenderId'] == $me) { ?>
						<article class="chat_message_me chat_message"><img src="<?php echo base_url() . 'uploads/'. $studentInfo[0]['padProfiel']; ?>" alt=""><p><?php echo $eenBericht['bericht']; ?></p></article>
				<?php } else if ($eenBericht['zenderId'] == $you) { ?>
				 <article class="chat_message_you chat_message"><img src="http://graph.facebook.com/<?php echo $bezoeker; ?>/picture?type=square" alt=""><p><?php echo $eenBericht['bericht']; ?></p></article> 
				<?php 
				}		
			} 
		?>
	
	</section>
	<?php } ?>

	<section class="chat_section_nieuwbericht">
			<form action="" method="post" id="chat_form">
			<input type="text" id="chat_inputbericht" class="form-control" placeholder="Wat wil je vragen?" name="text" autocomplete="off">
			<button id="chat_btnSend" type="submit" class="btn btn-primary" value="Send">Verstuur bericht</button>
			</form>
	</section>
	
	
	
</body>
<script type="text/javascript">
	$(document).ready(function(){
		var objDiv = $(".chat_box");
		var aantalkeer = 0;
		$("#chat_btnSend").on("click", function(e){
			var art = '';
			e.preventDefault();
			
			if (aantalkeer <= 0) {
				aantalkeer ++
				if(!$.trim($('#chat_inputbericht').val()) == ''){
					var bericht = $("#chat_inputbericht").val();
					$("#chat_inputbericht").val('');
					$.ajax({
		  			  	method: "POST",
					  	url: "<?php echo base_url(); ?>" + "index.php/chat/verwerkBerichtAjax/" + "<?php echo $me . '/' . $you . '/' . $bezoeker . '/' . $student . '/' . $chatId ?>",
					  	data: { "bericht": bericht }
					})
					 .done(function(res) {
					 	if(res.status === "success")
					    {
					    	<?php if ($loggedin == 'student') { ?>
					    		art += "<article class='chat_message_me chat_message'><img src= <?php echo base_url() . 'uploads/'. $studentInfo[0]['padProfiel']; ?> alt=''><p>"+ res.bericht + "</p></article>";
					    	<?php } else if ($loggedin == 'bezoeker') { ?>
					    		art += "<article class='chat_message_me chat_message'><img src= <?php echo 'http://graph.facebook.com/'. $bezoeker . '/picture?type=square'; ?>  alt=''><p>"+ res.bericht + "</p></article>";	
					    	<?php } ?>

					    	$(".chat_box").append(art);
					    	
							if (objDiv.length > 0){
		       				 	objDiv[0].scrollTop = objDiv[0].scrollHeight;
		   					}
					   }
					   aantalkeer --;
					});
				}
			}
		});

		function updateBerichten()
		{
			var art = [];
				$.ajax({
	  			  	method: "GET",
				  	url: "<?php echo base_url(); ?>" + "index.php/chat/updateBerichtenAjax/" + "<?php echo $me . '/' . $you . '/' . $bezoeker . '/' . $student . '/' . $chatId ?>"
				})
				 .done(function(res) {
				 	if (res.status === 'success') {
					 	for (var i = res.bericht.length - 1; i >= 0; i--) {
					 		<?php if ($loggedin == 'bezoeker') { ?>
					 			art[i] = "<article class='chat_message_you chat_message'><img src= <?php echo base_url() . 'uploads/'. $studentInfo[0]['padProfiel']; ?> alt=''><p>"+ res.bericht[i]['bericht'] + "</p></article>";
					 		<?php } else if ($loggedin == 'student') { ?>
					 			art[i] = "<article class='chat_message_you chat_message'><img src= <?php echo 'http://graph.facebook.com/'. $bezoeker . '/picture?type=square'; ?> alt=''><p>"+ res.bericht[i]['bericht'] + "</p></article>";
					 		<?php } ?>
					 	}
					 	$(".chat_box").append(art.join());
					 	if (objDiv.length > 0){
		       				 	objDiv[0].scrollTop = objDiv[0].scrollHeight;
		   				}
					 	console.log(res.status);
	   				}
				});
		}
		
		updateBerichten(); 
		setInterval(function(){ updateBerichten() }, 5000);


	});
</script>
</html>
	