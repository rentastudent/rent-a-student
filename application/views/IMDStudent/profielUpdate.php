<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Make a new profile</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">

</head>
<body class="student_profupdate_body">
  <?php $this->load->view('IMDStudent/nav.inc.php'); ?>

  <div class="row">
    <div class="row">
      <div class="col-xs-6 col-md-4"></div>
      <div class="col-xs-6 col-md-4"><h1>Profiel aanpassen</h1></div>
      <div class="col-xs-6 col-md-4"></div>
    </div>

    <?php 
    if($this->session->flashdata('profiel_error'))
    {
      echo $this->session->flashdata('profiel_error');
    } 
    ?>

    <?php foreach($profiel as $p): ?>

    <form action="" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col-xs-6 col-md-4"></div>
        <div class="col-xs-6 col-md-4">

          <!--<?php //echo form_open_multipart('IMDstudent/registratie');?>-->
          <div class="form-group">
            <label for="voornaam">Voornaam</label>
            <?php echo form_error('voornaam'); ?>
            <input type="text" class="form-control" id="voornaam" placeholder="Vul hier je voornaam in" name="voornaam" value="<?php echo $p['voornaam'];?>">
          </div>

          <div class="form-group">
            <label for="achternaam">Achternaam</label>
            <?php echo form_error('achternaam'); ?>
            <input type="text" class="form-control" id="achternaam" placeholder="Vul hier je achternaam in" name="achternaam" value="<?php echo $p['achternaam'];?>">
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <?php echo form_error('email'); ?>
            <input type="email" class="form-control" id="email" placeholder="Geef hier je emailadres dat zal dienen als username" name="email" value="<?php echo $p['email'];?>">
          </div>

          <div class="form-group">
            <label for="huidigPaswoord">Huidig paswoord</label>
            <?php echo form_error('paswoord'); ?>
            <input type="password" class="form-control" id="huidigPaswoord" placeholder="Geef hier je huidig wachtwoord in" name="huidigPaswoord" value="">
          </div>

          <div class="form-group">
            <label for="nieuwPaswoord">Nieuw paswoord</label>
            <?php echo form_error('paswoord'); ?>
            <input type="password" class="form-control" id="nieuwPaswoord" placeholder="Geef hier je gewenst wachtwoord in" name="nieuwPaswoord" value="">
          </div>

          <div class="form-group">
            <label for="confirmPaswoord">Confirmeer paswoord</label>
            <?php echo form_error('paswoord'); ?>
            <input type="password" class="form-control" id="confirmPaswoord" placeholder="Geef hier je gewenst wachtwoord in" name="confirmPaswoord" value="">
          </div>

          <div class="form-group">
            <label for="studiejaar">Studiejaar</label>
            <select class="form-control" id="studiejaar" name="studiejaar">
              <option value="1" <?php if($p['studiejaar'] == '1'): ?> selected="selected"<?php endif; ?>>1ste jaar</option>
              <option value="2" <?php if($p['studiejaar'] == '2'): ?> selected="selected"<?php endif; ?>>2de jaar</option>
              <option value="3" <?php if($p['studiejaar'] == '3'): ?> selected="selected"<?php endif; ?>>3de jaar</option>
            </select>
          </div>

          <div class="form-group">
            <label for="afstudeerrichting">Afstudeerrichting</label>
            <select class="form-control" id="afstudeerrichting" name="afstudeerrichting">
              <option value="webontwikkeling" <?php if($p['afstudeerrichting'] == 'webontwikkeling'): ?> selected="selected"<?php endif; ?>>Webontwikkeling</option>
              <option value="webdesign" <?php if($p['afstudeerrichting'] == 'webdesign'): ?> selected="selected"<?php endif; ?>>Webdesign</option>
            </select>
          </div>

          <div class="form-group">
            <label for="telefoonnummer">Telefoonummer</label>
            <?php echo form_error('telefoonnummer'); ?>
            <input type="tel" class="form-control" id="telefoonnummer" placeholder="Geef hier je telefoonnummer in" name="telefoonnummer" value="<?php echo $p['telefoonnummer'];?>">
          </div>

          <div class="form-group">
            <label for="woonplaats">Woonplaats</label>
            <?php echo form_error('woonplaats'); ?>
            <input type="text" class="form-control" id="woonplaats" placeholder="Geef hier je woonplaats in" name="woonplaats" value="<?php echo $p['woonplaats'];?>">
          </div>

          <div class="form-group">
            <label for="motivatie">Waarom heb je gekozen voor Interactive Multimedia Design?</label>
            <?php echo form_error('motivatie'); ?>
            <textarea class="form-control" id="motivatie" placeholder="Motiveer je keuze" name="motivatie"><?php echo $p['motivatie'];?></textarea>
          </div>
        </div>
        <div class="col-xs-6 col-md-4"></div>
      </div>

      <div class="row">
        <div class="col-xs-6 col-md-2"></div>
        <div class="col-xs-6 col-md-4">
         <div class="form-group updateProfielform">
          <label for="huidigeProfielFoto">Huidige Profielfoto</label>
        </div>
        <div class="form-group updateProfielform">
          <img src="<?php echo base_url().'uploads/'.$p['padProfiel'] ?>" alt="huidige profielfoto" id="huidigeProfielFotoUpdate">
        </div>
      </div>

      <div class="col-xs-6 col-md-4">
        <div class="form-group updateProfielform">
          <label for="userfile">Stel een nieuwe profielfoto in</label>
          <input type="file" name="userfile" size="20" id="uploadImage" onchange="PreviewImage();" class="updateProfielform">
        </div>  
        <div class="form-group updateProfielform">
          <img id="uploadPreview" src="<?php echo asset_url();?>images/placeholder.jpg"/>
        </div>
      </div>
      <div class="col-xs-6 col-md-2"></div>

      <div class="row">
        <div class="col-xs-6 col-md-4"></div>
        <div class="col-xs-6 col-md-4"><button type="submit" id="knopProfielAanpassen" class="btn btn-default updateProfielform btn-primary" name="upload">Aanpassen</button></div>
        <div class="col-xs-6 col-md-4"></div>
      </div>
    </form>

  <?php endforeach; ?>
</div>
<script src="<?php echo asset_url();?>js/imdRegistratie.js"></script>  
</body>
</html>