<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Make a new profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">

</head>
<body class="student_register_body">

<?php $this->load->view('gebruiker/nav.inc.php'); ?>

<div class="row">
<div class="col-xs-6 col-md-4"></div>
<div class="col-xs-6 col-md-4 content_container">

<h3>Registreer je als IMD Student/gids.</h3>
<form action="" method="post" enctype="multipart/form-data">
<!--<?php //echo form_open_multipart('IMDstudent/registratie');?>-->
  <div class="form-group">
    <label for="voornaam">Voornaam</label>
    <?php echo form_error('voornaam'); ?>
    <input type="text" class="form-control" id="voornaam" placeholder="Vul hier je voornaam in" name="voornaam" value="<?php echo set_value('voornaam'); ?>">
  </div>
    
  <div class="form-group">
    <label for="achternaam">Achternaam</label>
    <?php echo form_error('achternaam'); ?>
    <input type="text" class="form-control" id="achternaam" placeholder="Vul hier je achternaam in" name="achternaam" value="<?php echo set_value('achternaam'); ?>">
  </div>

   <div class="form-group">
    <label for="email">Email</label>
    <?php echo form_error('email'); ?>
    <input type="email" class="form-control" id="email" placeholder="Geef hier je emailadres dat zal dienen als username" name="email" value="<?php echo set_value('email'); ?>">
  </div>
    
  <div class="form-group">
    <label for="paswoord">Paswoord</label>
    <?php echo form_error('paswoord'); ?>
    <input type="password" class="form-control" id="paswoord" placeholder="Geef hier je gewenst wachtwoord in" name="paswoord" value="<?php echo set_value('paswoord'); ?>">
  </div>
    
  <div class="form-group">
      <label for="studiejaar">Studiejaar</label>
      <select class="form-control" id="studiejaar" name="studiejaar">
          <option value="1" <?php echo set_select('studiejaar', '1', TRUE); ?>>1ste jaar</option>
          <option value="2" <?php echo set_select('studiejaar', '2'); ?>>2de jaar</option>
          <option value="3" <?php echo set_select('studiejaar', '3'); ?>>3de jaar</option>
      </select>
  </div>
  
  <div class="form-group">
      <label for="afstudeerrichting">Afstudeerrichting</label>
      <select class="form-control" id="afstudeerrichting" name="afstudeerrichting">
          <option value="webontwikkeling" <?php echo set_select('afstudeerrichting', 'webontwikkeling', TRUE); ?>>Webontwikkeling</option>
          <option value="webdesign" <?php echo set_select('afstudeerrichting', 'webdesign'); ?>>Webdesign</option>
      </select>
  </div>

  <div class="form-group">
    <label for="telefoonnummer">Telefoonummer</label>
    <?php echo form_error('telefoonnummer'); ?>
    <input type="tel" class="form-control" id="telefoonnummer" placeholder="Geef hier je telefoonnummer in" name="telefoonnummer" value="<?php echo set_value('telefoonnummer'); ?>">
  </div>

  <div class="form-group">
    <label for="woonplaats">Woonplaats</label>
    <?php echo form_error('woonplaats'); ?>
    <input type="text" class="form-control" id="woonplaats" placeholder="Geef hier je woonplaats in" name="woonplaats" value="<?php echo set_value('woonplaats'); ?>">
  </div>

   <div class="form-group">
    <label for="motivatie">Waarom heb je gekozen voor Interactive Multimedia Design?</label>
    <?php echo form_error('motivatie'); ?>
    <textarea class="form-control" id="motivatie" placeholder="Motiveer je keuze" name="motivatie"><?php echo set_value('motivatie');?></textarea>
  </div>
  
  <div class="form-group">
    <label for="userfile">Stel een profielfoto in</label>
    <input type="file" name="userfile" size="20" id="uploadImage" onchange="PreviewImage();">
  </div>
  <div class="form-group">
    <img id="uploadPreview" src="<?php echo asset_url();?>images/placeholder.jpg"/>
  </div>

  <button type="submit" class="btn btn-primary" name="upload">Aanmaken</button>
</form>
</div>
<div class="col-xs-6 col-md-4"></div>
</div>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="<?php echo asset_url();?>js/imdRegistratie.js"></script>
</body>
</html>