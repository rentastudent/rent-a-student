<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home | Rent-A-Student</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="studenthome_body">
	<?php $this->load->view('IMDStudent/nav.inc.php'); ?>
	<?php 
	if($this->session->flashdata('data'))
	{
	?>
		<div class="alert alert-success">
	    <?php echo $this->session->flashdata('data');?>
		</div>
	<?php	
	} 
	?>	
	<h1 class="welcome_user">Welkom <?php echo $this->session->userdata('naam') ?>!</h1>

	<div class="home_dashboard">
			<a href="<?php echo site_url('imdstudent/datumgidsen') ?>" class="home_links lefts"><h3><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><br>Kies data</h3></a>
			<a href="<?php echo site_url('imdstudent/profielaanpassen') ?>" class="home_links bottom"><h3><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><br>Update profiel</h3></a>
			<div class="home_bezoeken">
				<h3>Uw bezoeken</h3>
				<div class="panel panel-default">
				<table class="table table-striped">
					<tr>
					    <th>Datum</th>
					    <th>Uur</th> 
					    <th>Bezoekernaam</th>
					    <th>Chat</th>
					</tr>

					<?php  
						foreach ($bezoeken as $key => $value) : ?>
							<?php 
							$daynumber = date("N", strtotime($value["datum"]));
							if($daynumber == 1)
							{
								$day = "Maandag";
							}
							else if ($daynumber == 2) {
								$day = "Dinsdag";
							}
							else if ($daynumber == 3) {
								$day = "Woensdag";
							}
							else if ($daynumber == 4) {
								$day = "Donderdag";
							}
							else if ($daynumber == 5) {
								$day = "Vrijdag";
							}
							else if ($daynumber == 6) {
								$day = "Zaterdag";
							}
							else if ($daynumber == 7) {
								$day = "Zondag";
							}
							$date = date("d/m/Y", strtotime($value["datum"]));
							echo "<tr><td>" . $day . ", " . $date . "</td><td>"?>
							<?php echo $value["uur"] . " uur</td><td>"; ?>
							<?php if ($value["BezoekerId"] == array_search($value["BezoekerId"], array_column($studenten, 'id'))) {
										echo "<img src='https://graph.facebook.com/" . $studenten[$value["BezoekerId"]]["fbuserid"] . "/picture'>" . $studenten[$value["BezoekerId"]]["voornaam"] ."</td><td>". $studenten[$value["BezoekerId"]]["achternaam"] . "</td><td>". $bezoekers[$value["BezoekerId"]]["email"];
									} else{
										echo "<img src='https://graph.facebook.com/" . $studenten[array_search($value["BezoekerId"], array_column($studenten, 'id'))]["fbuserid"] . "/picture'>" . $studenten[array_search($value["BezoekerId"], array_column($studenten, 'id'))]["voornaam"] . " " . $studenten[array_search($value["BezoekerId"], array_column($studenten, 'id'))]["achternaam"] ."</td><td><a class='chat_met_button' href='" . base_url() . "chat/bericht/" . $studenten[array_search($value["BezoekerId"], array_column($studenten, 'id'))]["fbuserid"] . "/" . $value["IMDStudentId"] . "'>Maak afspraken.</a></td>";
									}?>
					<?php echo "</tr>"; endforeach; ?> 
				</table>
			</div>
			</div>

	</div>
</body>
</html>