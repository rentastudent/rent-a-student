<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Datum kiezen om te gidsen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<style>

	.datum {
		float: left;
		margin-right: 5%;
		width: 44%;
		font-size: 20px;
	}

	</style>
</head>
<body class="student_data_body">
	<?php $this->load->view('IMDStudent/nav.inc.php'); ?>
	<div class="row">
		<div class="col-md-12 col-md-3"></div>
		<div class="col-md-12 col-md-8 content_container">
				<h2>Kies de data waarop jij gids kan zijn voor een bezoeker</h2>
			<?php if(isset($errorValidatie)) {
				echo $errorValidatie;
			} ?>
			<br><br>
			<form class="form-horizontal" method="post" action="">
				<?php  
				foreach ($dates as $key => $d) : ?>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="row">
								<div class="col-md-8, datum"><?php echo date("l", strtotime($d['datum'])).", ";?><small><?php echo $d['datum']?></small></div>
								<div class="col-md-4, praktischeInfo"><input type="checkbox" <?php
								
								if(isset($opgeslagen)){
									foreach ($opgeslagen as $lee => $o) {
									if($o['DatumId'] == $d['id']) {
										echo 'checked';
									}
								}
								}
								?>
								 name="check_list[]" value="<?php echo $d['id']?>"></div>
							</div>

						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-8">
					<button type="submit" class="btn btn-primary">Stel je beschikbaar</button>
				</div>
			</form>
		</div>
		<div class="col-md-6 col-md-2"></div>
	</div> 
</body>
</html>




