<header class="student_header">
	<div class="RAS_logo">
		<a href="<?php echo site_url('') ?>" class="header_logo">RENT-A-STUDENT</a>
	</div>
	<nav class="student_nav nav_bg">
		<a href="<?php echo site_url('IMDStudent/home') ?>" class="nav_link">Home</a>
		<a href="<?php echo site_url('IMDStudent/datumgidsen') ?>" class="nav_link">Kies data</a>
		<a href="<?php echo site_url('IMDStudent/profielAanpassen') ?>" class="nav_link">Update profiel</a>
	</nav>
	<div class="nav_bg">
		<a href="<?php echo site_url('gebruiker/logout') ?>" class="login_btn">Uitloggen</a>
	</div>
</header>

