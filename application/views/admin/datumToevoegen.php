<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>datumToevoegen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="login_body">
	<?php $this->load->view('admin/nav.inc.php'); ?>
	
	<div class="content_container">
	<form action="" method="post">
		<h2 style="margin-top:0px">Voeg een datum toe:</h2>
		<input type="date" class="form-control" name="date" placeholder="new date">
		<br>
		<button type="submit" name="toevoegen" class="btn btn-primary">Voeg datum toe</button>
	</form>

	<!-- list met tweets -->
	<h2>Alle bezoekdagen:</h2>
	<ul class="list-group">
	<?php  
		foreach ($dates as $d) : ?>
		<?php
		$daynumber = date("N", strtotime($d['datum']));
							if($daynumber == 1)
							{
								$day = "Maandag";
							}
							else if ($daynumber == 2) {
								$day = "Dinsdag";
							}
							else if ($daynumber == 3) {
								$day = "Woensdag";
							}
							else if ($daynumber == 4) {
								$day = "Donderdag";
							}
							else if ($daynumber == 5) {
								$day = "Vrijdag";
							}
							else if ($daynumber == 6) {
								$day = "Zaterdag";
							}
							else if ($daynumber == 7) {
								$day = "Zondag";
							}
		$date = date("d/m/Y", strtotime($d['datum']));?>
		<?php  $id = $d['id'];?>
		<?php echo "<li class='list-group-item " . $d['id'] . "'><div class='row'><div class='col-md-11'>" . $day. ", " . $date . " </div><div class='col-md-1'><a href='" . site_url("admin/datumverwijderen/$id") . "' data-id='" . $id . "' class='delete'>Verwijder</a></div></li>"; ?>
	<?php endforeach; ?> 
	</ul>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
				$('.delete').on("click", function(e)
				{
					var p_id = $(this).attr("data-id")
					var href = $(this).attr("href");
					e.preventDefault();

				$.ajax({
				  type: "GET",
				  url: href
				  })
				  .done(function( res ) {
				  	$('.' + p_id).slideUp('fast');
				  });

				});

			});
	</script>
</body>
</html>