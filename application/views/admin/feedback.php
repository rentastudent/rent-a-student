<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Feedback</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="feedback_body">
	<?php $this->load->view('admin/nav.inc.php'); ?>
	
	<div class="content_container">
	<div class="panel panel-default">
	  <div class="panel-heading">Alle feedback:</div>
		 <table class="table table-striped">
		<?php  
			foreach ($feedback as $f) : ?>
			<?php 
				$aanraden = $f['aanraden'];
				if($aanraden == 1)
				{
					$aanraden = "Ja";
				}
				else if($aanraden == 0)
				{
					$aanraden = "Nee";
				}
			?>
			<?php echo "<tr><td><h3>Naam: </h3>" . $f['voornaam'] . ' ' . $f['achternaam'] . "</br>" . "<h3>Email: </h3>" . $f['email'] . "</br>" . "<div class='feedback_space'><h3>Sfeer: </h3>" . $f['sfeer'] . "</br><h3>Contact met gids: </h3>" . $f['contact'] . "</br><h3>Indruk over de applicatie: </h3>" . $f['indruk'] . "</br><h3>Applicatie al dan niet aan raden: </h3>" . $aanraden . "</div>" . "<div class='feedback_space'><h3>Wat beter kan: </h3></br>" . $f['verbeteringen'] . "</div>" . "<div class='feedback_space'><h3>Wat positief is: </h3></br>" . $f['positief'] . "</div></br>" . "<a href='mailto:" . $f['email'] . "?Subject=Thanks%20for%20the%20feedback!' target='_top'>Contacteer deze bezoeker</a>" . "</td></tr>"; ?>
		<?php endforeach; ?> 
		</table>
	</div>
	</div>
</body>
</html>