<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rent-A-Student: Admin - Boekingen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="admin_boekingen_body">
	
	<?php $this->load->view('admin/nav.inc.php'); ?>

		<div class="content_container">
			<div class="panel panel-default">
			<table class="table table-striped">
		    <div class="panel-heading"><h1>Boekingen:</h1></div>
			 	<tr>
				    <th>IMD-Student</th>
				    <th>Info boeking</th> 
				    <th>Bezoeker</th>
				</tr>

			<?php
			foreach ($bezoeken as $key => $value) {
			?>

			<tr>
				<td>
						<div>
							<img class='boekingenlijst_profielfoto' src="<?php echo base_url();?>uploads/<?php 
								if ($value["IMDStudentId"] == array_search($value["IMDStudentId"], array_column($profielen, 'id'))) {
									echo $profielen[$value["IMDStudentId"]]["padProfiel"];
								} else{
									echo $profielen[array_search($value["IMDStudentId"], array_column($profielen, 'id'))]["padProfiel"];
								} 
							?>" alt="profielfoto">
							<div class="boeking_info">
							<p>
								<?php if ($value["IMDStudentId"] == array_search($value["IMDStudentId"], array_column($profielen, 'id'))) {
									echo $profielen[$value["IMDStudentId"]]["voornaam"] ." ". $profielen[$value["IMDStudentId"]]["achternaam"];
								} else{
									echo $profielen[array_search($value["IMDStudentId"], array_column($profielen, 'id'))]["voornaam"] ." ". $profielen[array_search($value["IMDStudentId"], array_column($profielen, 'id'))]["achternaam"];
								} ?>
							</p>
							<p>
								<?php if ($value["IMDStudentId"] == array_search($value["IMDStudentId"], array_column($profielen, 'id'))) {
									echo $profielen[$value["IMDStudentId"]]["studiejaar"];
								} else{
									echo $profielen[array_search($value["IMDStudentId"], array_column($profielen, 'id'))]["studiejaar"];
								} ?>
								IMD
							</p>
							</div>
						</div>
				</td>

				<td>
					<div class="boeking_info">
						<p><strong>Datum:</strong> <?php echo $value["datum"] ?></p>
						<p><strong>Uur:</strong> <?php echo $value["uur"] ?> uur</p>
					</div>
				</td>

				<td>
					<div>
						<img class='boekingenlijst_profielfoto' src="http://graph.facebook.com/
							<?php if ($value["BezoekerId"] == array_search($value["BezoekerId"], array_column($bezoekers, 'id'))) {
								echo $bezoekers[$value["BezoekerId"]]["fbuserid"];
							} else{
								echo $bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["fbuserid"];
							} ?>
						/picture?type=normal" alt="profielfoto">
						<div class="boeking_info">
						<p>
							<?php if ($value["BezoekerId"] == array_search($value["BezoekerId"], array_column($bezoekers, 'id'))) {
								echo $bezoekers[$value["BezoekerId"]]["voornaam"] ." ". $bezoekers[$value["BezoekerId"]]["achternaam"];
							} else{
								echo $bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["voornaam"] ." ". $bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["achternaam"];
							} ?>
						</p>
						<p>Bezoeker</p>
						</div>
					</div>
					
				</td>
			</tr>

			<?php 
				}
			?>
			</table>
		
		</div>
	</div>	
</body>
</html>