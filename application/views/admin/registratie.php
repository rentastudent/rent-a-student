<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rent-A-Student: Admin - Registratie</title>
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body class="admin_registreer_body">
	<?php $this->load->view('admin/nav.inc.php'); ?>
	<div class="row">
		<div class="col-xs-6 col-md-4">
		</div>

		<div class="col-xs-6 col-sm-4 content_container">
			<?php echo validation_errors(); ?>

			<form action="" method="post">
				<div class="form-group">
					<h2>Registratie Admin:</h2>
					<label for="voornaam">Voornaam</label>
					<input id="voornaam" name="voornaam" class="form-control" value="<?php echo set_value('voornaam'); ?>" type="text">

					<label for="achternaam">Achternaam</label>
					<input id="achternaam" name="achternaam" class="form-control" value="<?php echo set_value('achternaam'); ?>" type="text">

					<label for="emailadres">E-mailadres</label>
					<input id="emailadres" name="emailadres" class="form-control" value="<?php echo set_value('emailadres'); ?>" type="text">

					<label for="paswoord">Paswoord</label>
					<input id="paswoord" name="paswoord" class="form-control" value="<?php echo set_value('paswoord'); ?>" type="password">

					<label for="bevpaswoord">Bevestiging paswoord </label>
					<input id="bevpaswoord" name="bevpaswoord" class="form-control" value="<?php echo set_value('bevpaswoord'); ?>" type="password">

					<button class="btn btn-primary" name="registreer" type="submit">Registreer Administrator</button>
					<br>
					<h3>Verwijder Admin:</h3>
					<label for="v_emailadres">E-mailadres</label>
					<input id="v_emailadres" name="v_emailadres" class="form-control" value="<?php echo set_value('v_emailadres'); ?>" type="text">
					<button class="btn btn-primary" name="verwijder" type="submit">Verwijder Administrator</button>
				</div>
			</form>
		</div>


		<div class="col-xs-6 col-md-4">
		</div>
	</div>	
</body>
</html>