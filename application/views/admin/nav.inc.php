<header class="admin_header">
	<div class="RAS_logo">
		<a href="<?php echo site_url('') ?>" class="header_logo">RENT-A-STUDENT</a>
	</div>
	<nav class="admin_nav nav_bg">
		<a href="<?php echo site_url('admin/home') ?>" class="nav_link">Home</a>
		<a href="<?php echo site_url('admin/bezoekerlijst') ?>" class="nav_link">Bezoekerlijst</a>
		<a href="<?php echo site_url('admin/boekingen') ?>" class="nav_link">Bekijk boekingen</a>
		<a href="<?php echo site_url('admin/datumtoevoegen') ?>" class="nav_link">Datum toevoegen</a>
		<a href="<?php echo site_url('admin/registratie') ?>" class="nav_link">Registreer/verwijder admin</a>
		<a href="<?php echo site_url('admin/feedback') ?>" class="nav_link">Bekijk feedback</a>
	</nav>
	<div class="nav_bg">
		<a href="<?php echo site_url('gebruiker/logout') ?>" class="login_btn">Uitloggen</a>
	</div>
</header>

