<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>datumToevoegen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="form_body">
	<?php $this->load->view('admin/nav.inc.php'); ?>
	<div class="content_container">
	<!-- list met bezoeken -->
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading"><h1>Bezoekers:</h1></div>
		 <table class="table table-striped">
		 	<tr>
			    <th>Datum</th>
			    <th>Uur</th> 
			    <th>Voornaam</th>
			    <th>Achternaam</th>
			    <th>Email</th>
			  </tr>
		

		<?php  
			foreach ($bezoeken as $key => $value) : ?>
				<?php echo "<tr><td>" . $value["datum"] . "</td><td>"  ?>
				<?php echo $value["uur"] . "</td><td>"; ?>
				<?php if ($value["BezoekerId"] == array_search($value["BezoekerId"], array_column($bezoekers, 'id'))) {
							echo $bezoekers[$value["BezoekerId"]]["voornaam"] ."</td><td>". $bezoekers[$value["BezoekerId"]]["achternaam"] . "</td><td>". $bezoekers[$value["BezoekerId"]]["email"];
						} else{
							echo $bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["voornaam"] ."</td><td>". 
							$bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["achternaam"] . "</td><td>". 
							$bezoekers[array_search($value["BezoekerId"], array_column($bezoekers, 'id'))]["email"] . "</td>";
						} ?>
		<?php echo "</tr>"; endforeach; ?> 
		</table>
	</div>
	</div>

</body>
</html>