<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Niet bevoegd</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="loginerror_body">

	<header>
		<a class="header_logo" href="<?php echo site_url() ?>">RENT-A-STUDENT</a>
	</header>

	<div class="login_container">
		<h2>U bent niet bevoegd om deze pagina te bekijken.</h2>
		<a href="<?php echo site_url('gebruiker/login') ?>">Log in.</a>
	</div>
</body>
</html>