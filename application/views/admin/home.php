<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home | Admin</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="adminhome_body">
	
	<?php $this->load->view('admin/nav.inc.php'); ?>
	<?php 
	if($this->session->flashdata('admin_register'))
	{?>
		<div class="alert alert-success">
		<?php echo $this->session->flashdata('admin_register');?>
		</div>
	<?php
	} 
	?>

	<h1 class="welcome_user">Welkom <?php echo $this->session->userdata('naam') ?>!</h1>

	<div class="home_dashboard">
			<a href="<?php echo site_url('admin/bezoekerlijst') ?>" class="home_links lefts"><h3><span class="glyphicon glyphicon-list" aria-hidden="true"></span><br>Bezoekerlijst</h3></a>
			<a href="<?php echo site_url('admin/boekingen') ?>" class="home_links"><h3><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><br>Bekijk boekingen</h3></a>
			<a href="<?php echo site_url('admin/datumtoevoegen') ?>" class="home_links lefts"><h3><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><br>Datum toevoegen</h3></a>
			<a href="<?php echo site_url('admin/registratie') ?>" class="home_links"><h3><span class="glyphicon glyphicon-user" aria-hidden="true"></span><br>Registreer/verwijder admin</h3></a>
			<a href="<?php echo site_url('admin/feedback') ?>" class="home_links center"><h3><span class="glyphicon glyphicon-comment" aria-hidden="true"></span><br>Bekijk feedback</h3></a>
	</div>

</body>
</html>