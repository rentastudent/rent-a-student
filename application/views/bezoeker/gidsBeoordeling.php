<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Beoordeling IMD-gids</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">

			<h2>Beoordeling gids</h2>
			<br>
			<p>Beste bezoeker. Hopelijk was je tevreden over je bezoek aan de IMD-campus. Wij vinden het belangrijk om mee te delen aan de gidsen wat jullie vonden van het bezoek aan onze campus. Het is belangrijk om zo eerlijk een antwoord te geven. Aan de hand van de verschillende vragen krijgen de gidsen een waardering. Gelieve ook je bezoek te beschrijven in één quote.</p>
			<?php if(isset($errorValidatie)) { ?>
				<p style="color:red"><?php echo $errorValidatie;?></p>
			<?php } ?>
			<form role="form" method="post" action="">
				<h4>Kon de gids je goed informeren over de verschillende vakken?</h4>
				<label class="radio-inline">
					<input type="radio" name="vakken" value="5">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="vakken" value="4">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="vakken" value="3">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="vakken" value="2">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="vakken" value="1">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Kon de gids je goed rondleiden over de campus?</h4>
				<label class="radio-inline">
					<input type="radio" name="campus" value="5">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="campus" value="4">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="campus" value="3">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="campus" value="2">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="campus" value="1">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Heeft de gids je betrokken bij sociale contacten met andere studenten?</h4>
				<label class="radio-inline">
					<input type="radio" name="sociaal" value="5">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="sociaal" value="4">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="sociaal" value="3">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="sociaal" value="2">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="sociaal" value="1">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Hoe verliep de communicatie met de gids?</h4>
				<label class="radio-inline">
					<input type="radio" name="communicatie" value="5">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="communicatie" value="4">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="communicatie" value="3">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="communicatie" value="2">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="communicatie" value="1">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Beschrijf je bezoek in één zin:</h4>
  				<textarea class="form-control" rows="5" id="positief" name="quote"></textarea>
				<br>
				<br>
				<button type="submit" class="btn btn-default">Versturen</button>
			</form>
		</div>
		<div class="col-md-2"></div>
	</div>
</body>
</html>