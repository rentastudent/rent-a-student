<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>maak een bezoek aan!</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="form_body">
	<?php $this->load->view('bezoeker/nav.inc.php'); ?>
	
	<div class="content_container">
		<?php echo validation_errors(); ?>
	<form action="" method="post">
		<h2 style="margin-top:0px;">Beschikbare data</h2>
		<label for="Datum">Kies op welke dag je wil komen</label>
		<select name="Datum" class="form-control">
			<?php  
				foreach ($dates as $d) : ?>
				<?php echo "<option value='" . $d['datum'] . "'>" . $d['datum'] . "</option>"; ?>
			<?php endforeach; ?>
		</select><br>
		<label for="Uur">Kies op welk uur je wil komen</label>
		<select name="Uur" class="form-control">
			<option value="9" selected>9 uur</option>
			<option value="11">11 uur</option>
			<option value="13">13 uur</option>
			<option value="15">15 uur</option>
		</select><br>
		<h2>Beschikbare gidsen</h2>
		<label for="student">Kies hier uw gids</label>
		<select name="student" class="form-control">
			<?php foreach($profielen as $p): ?>
			<?php echo "<option value='" . $p['id'] . "'>" . $p['voornaam'] . " " . $p['achternaam'] . "</option>"; ?>
			<?php endforeach; ?>
		</select>
		<br>
		<?php  
				foreach ($bezoeker as $br) : ?>
				<?php echo "<input style='display:none' readonly class='form-control' name='bezoekerId' value='" . $br['id'] . "'><br>"; ?>
			<?php endforeach; ?>
		<button type="submit" class="btn btn-primary">Reserveer bezoek</button>
	</form>
	</div>
</body>
</html>