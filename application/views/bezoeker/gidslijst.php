<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lijst van gidsen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="gidslijst_body">
	<?php $this->load->view('bezoeker/nav.inc.php'); ?>
	<div class="content_container">
			<h2>Kies een gids.</h2>
	<div class="panel panel-default">
	  <div class="panel-heading">Lijst van alle gidsen:</div>
		 <table class="table">
		<?php  
			foreach ($profielen as $p) : ?>
			<?php echo "<tr><td><div><a href='" . site_url('bezoeker/gidsprofiel') . "/" . $p['id'] . "'>
			<div class='grow pic'><img class='gidslijst_img' src='" . base_url() . "uploads/" . $p['padProfiel'] . "''alt='Profielfoto' width='100'></img></div></a></div>
			<div class='gidslijst_details'><h3>". $p['voornaam'] . " " . $p['achternaam'] . "</h3>
		</br><p>Studiejaar: " . $p['studiejaar'] . "</p><a href='" . site_url('bezoeker/gidsprofiel') . "/" . $p['id'] . "'>Bekijk profiel</a></div></td></tr>"; ?>
		<?php endforeach; ?> 
		</table>
	</div>
	</div>
</body>
</html>