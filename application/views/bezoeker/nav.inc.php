<header class="bezoeker_header">
	<div class="RAS_logo">
		<a href="<?php echo site_url('') ?>" class="header_logo">RENT-A-STUDENT</a>
	</div>
	<nav class="bezoeker_nav nav_bg">
		<a href="<?php echo site_url('bezoeker/home') ?>" class="nav_link">Home</a>
		<a href="<?php echo site_url('bezoeker/gidslijst') ?>" class="nav_link">Lijst van gidsen</a>
		<a href="<?php echo site_url('bezoeker/feedbackBezoek') ?>" class="nav_link">Geef feedback</a>
		<a href="<?php echo site_url('bezoeker/popular') ?>" class="nav_link">Populairste gids</a>
	</nav>
	<div class="nav_bg">
		<a href="<?php echo site_url('gebruiker/logout') ?>" class="login_btn">Uitloggen</a>
	</div>
</header>

