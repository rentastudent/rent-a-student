<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Feedback Bezoeker</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="feedback_give_body">

	<?php $this->load->view('bezoeker/nav.inc.php'); ?>

<div class="feedback_container">
			<h2>Feedback Bezoeker op bezoek IMD-campus</h2>
			<br>
			<p>Beste bezoeker. Wij hopen dat de meeloopdag in de richting IMD het waard was. Om onze dienst te verbeteren is het belangrijk dat je ons vertelt over je ervaring. Gelieve zo eerlijk mogelijk het formulier in te vullen.</p>
			<?php if(isset($errorValidatie)) { ?>
				<p style="color:red"><?php echo $errorValidatie;?></p>
			<?php } ?>
			<form role="form" method="post" action="">
				<h4>Wat vond je van de algemene sfeer op de campus?</h4>
				<label class="radio-inline">
					<input type="radio" name="sfeer" value="Zeer goed">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="sfeer" value="Goed">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="sfeer" value="Ok">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="sfeer" value="Slecht">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="sfeer" value="Zeer slecht">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Wat vond je van het persoonlijk contact met je gids?</h4>
				<label class="radio-inline">
					<input type="radio" name="contact" value="Zeer goed">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact" value="Goed">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact" value="Ok">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact" value="Slecht">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact" value="Zeer slecht">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Wat is je algemene indruk van de Rent-a-student applicatie?</h4>
				<label class="radio-inline">
					<input type="radio" name="indruk" value="Zeer goed">Zeer goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="indruk" value="Goed">Goed
				</label>
				<label class="radio-inline">
					<input type="radio" name="indruk" value="Ok">Ok
				</label>
				<label class="radio-inline">
					<input type="radio" name="indruk" value="Slecht">Slecht
				</label>
				<label class="radio-inline">
					<input type="radio" name="indruk" value="Zeer slecht">Zeer slecht
				</label>
				<br>
				<br>
				<h4>Zou je de applicatie aanraden aan een vriend/vriendin?</h4>
				<label class="radio-inline">
					<input type="radio" name="aanraden" value="ja">Ja
				</label>
				<label class="radio-inline">
					<input type="radio" name="aanraden" value="nee">Nee
				</label>
				<br>
				<br>
				<label for="comment">Wat kan er beter?</label>
  				<textarea class="form-control" rows="5" id="verbeteringen" name="tekst-verbeteringen"></textarea>
				<br>
				<br>
				<label for="comment">Wat vond je positief?</label>
  				<textarea class="form-control" rows="5" id="positief" name="tekst-positief"></textarea>
  				<br>
				<br>
				<label for="comment">Mogen we je feedback gebruiken in onze communicatie?</label>
				<input type="hidden" name="toestemming" value="0" />
				<input type="checkbox" name="toestemming" value="1">
				<br>
				<br>
				<button type="submit" class="btn btn-primary">Insturen</button>
			</form>

</div>
</body>
</html>