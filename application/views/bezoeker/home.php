<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home | Bezoeker</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="bezoeker_home_body">
	<?php $this->load->view('instaModal.inc.php'); ?>

	<?php $this->load->view('bezoeker/nav.inc.php'); ?>

	<?php $user = $this->facebook->get_user(); ?>

	<?php 
	if($this->session->flashdata('success'))
	{ ?>
	<div class="alert alert-success">
	    <strong>Bedankt!</strong> <?php echo $this->session->flashdata('success'); ?>
	</div>
	<?php } ?>

	<h1 class="welcome_user">Welkom <?php echo $user['name'] ?>!</h1>

	<div class="home_dashboard">
		<a href="<?php echo site_url('bezoeker/gidslijst') ?>" class="home_links lefts"><h3><span class="glyphicon glyphicon-list" aria-hidden="true"></span><br>Lijst van gidsen</h3></a>
		<a href="<?php echo site_url('bezoeker/feedbackbezoek') ?>" class="home_links"><h3><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><br>Geef feedback</h3></a>
		<a href="<?php echo site_url('bezoeker/popular') ?>" class="home_links center"><h3><span class="glyphicon glyphicon-star" aria-hidden="true"></span><br>Populairste gids</h3></a>
		<div class="home_bezoeken">
				<h3>Uw bezoeken</h3>
				<div class="panel panel-default">
				<table class="table table-striped">
					<tr>
					    <th>Datum</th>
					    <th>Uur</th> 
					    <th>Gidsnaam</th>
					    <th>Chat</th>
					</tr>
					<?php  
						foreach ($bezoeken as $key => $value) : ?>
							<?php 
							$daynumber = date("N", strtotime($value["datum"]));
							if($daynumber == 1)
							{
								$day = "Maandag";
							}
							else if ($daynumber == 2) {
								$day = "Dinsdag";
							}
							else if ($daynumber == 3) {
								$day = "Woensdag";
							}
							else if ($daynumber == 4) {
								$day = "Donderdag";
							}
							else if ($daynumber == 5) {
								$day = "Vrijdag";
							}
							else if ($daynumber == 6) {
								$day = "Zaterdag";
							}
							else if ($daynumber == 7) {
								$day = "Zondag";
							}
							$date = date("d/m/Y", strtotime($value["datum"]));
							echo "<tr><td>" . $day . ", " . $date . "</td><td>"  ?>
							<?php echo $value["uur"] . " uur</td><td>"; ?>
							<?php if ($value["IMDStudentId"] == array_search($value["IMDStudentId"], array_column($studenten, 'id'))) {
										echo "<img src='". base_url() . "uploads/" . $studenten[$value["IMDStudentId"]]["padProfiel"] . "'alt='Profielfoto' width='50px' height='50px'>" . $studenten[$value["IMDStudentId"]]["voornaam"] ."</td><td>". $studenten[$value["IMDStudentId"]]["achternaam"] . "</td><td>". $bezoekers[$value["IMDStudentId"]]["email"];
									} else{
										echo "<a href='" . base_url() . "index.php/bezoeker/gidsprofiel/" . $value["IMDStudentId"] . "'><img src='". base_url() . "uploads/" . $studenten[array_search($value["IMDStudentId"], array_column($studenten, 'id'))]["padProfiel"] . "'alt='Profielfoto' width='50'>" . $studenten[array_search($value["IMDStudentId"], array_column($studenten, 'id'))]["voornaam"] . " " . $studenten[array_search($value["IMDStudentId"], array_column($studenten, 'id'))]["achternaam"] ."</a></td><td><a class='chat_met_button' href='" . base_url() . "chat/bericht/" . $user['id'] . "/" . $value["IMDStudentId"] . "'>Maak afspraken.</a></td>";
									}?>
					<?php echo "</tr>"; endforeach; ?>  
				</table>
			</div>
			</div>

	</div>
</body>
</html>
