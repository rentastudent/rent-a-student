<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Populairste Gidsen</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</head>
<body class="student_registersucces_body">
	<?php $this->load->view('bezoeker/nav.inc.php'); ?>
	<div class="content_container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-md-1"></div>
			<div class="col-md-12 col-md-10">
			<div class="col-md-12 col-md-10"><h1>Populairste gidsen</h1><br><p>Hieronder vind u een ranglijst van de gidsen die het beste scoorden op de beoordeling van onze bezoekers</p></div>
			<div class="col-md-6 col-md-1"></div>
				<?php 
				$i = 1;
				foreach($rankings as $r){?>
				<div class="col-md-3">
					<h2>Plaats: <?php echo $i ?></h2>
					<a href="<?php echo base_url() . "index.php/bezoeker/gidsprofiel/" . $r["id"]?>"><h3><?php echo $r['voornaam']?> <?php echo $r['achternaam']?></h3></a>
					<p> Gemiddelde score: <strong><?php echo $r['Gemiddelde']?></strong></p>
					<p> Totale score: <?php echo $r['Totaal'] ?></p>
					<p> Aantal rondleidingen: <?php echo $r['rondleidingen'] ?></p>
				</div>
				<?php $i++; }?>
			</div>
			<div class="col-md-6 col-md-1"></div>
		</div>
	</div>
	</div>

</body>
</html>
