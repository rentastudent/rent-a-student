<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lijst van gidsen</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Voces' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo asset_url();?>css/screen.css">
</head>
<body class="gidsprofiel_body">

	<?php if($loggedin == 'nee'){ 
			$this->load->view('nav.inc.php');
		} 
		else if($loggedin == 'student')
		{ 
			$this->load->view('IMDStudent/nav.inc.php'); 
		} 
		else if($loggedin == 'admin')
		{ 
			$this->load->view('admin/nav.inc.php');
		} 
		else if($loggedin == 'bezoeker')
		{ 
			$this->load->view('bezoeker/nav.inc.php'); 
		} 
		?>
	
	<div class="content_container">
		<?php  if (isset($profiel)) { foreach ($profiel as $p) : $user = $p['voornaam'] . " " . $p['achternaam'];?>
		<div class="row">
		    <div class="col-md-12 col-xs-12">
		      <div class="well panel panel-default">
		        <div class="panel-body">
		          <div class="row">
		            <div class="col-xs-12 col-sm-4 text-center">
		              <?php echo "<img class='gidsprofiel_img center-block img-rounded img-thumbnail img-responsive' src='" . base_url() . "uploads/" . $p['padProfiel'] . "''alt='Profielfoto' width='100'></img>" ?>
		            </div>
		            <!--/col--> 
		            <div class="col-xs-12 col-sm-8">
		              <h2><?php echo $p['voornaam'] . " " . $p['achternaam'] ?></h2>
		              <p><strong>Studiejaar: </strong><?php echo $p['studiejaar'] . "IMD" ?>  </p>
		              <p><strong>Richting: </strong><?php echo $p['afstudeerrichting'] ?></p>
		              <p><strong>Woonplaats: </strong><?php echo $p['woonplaats'] ?></p>
		              <p><strong>Waarom ik IMD studeer: </strong><br><?php echo $p['motivatie'] ?></p>
		            </div>
		            <!--/col-->          
		          </div>
		          <!--/row-->
		        </div>
		        <!--/panel-body-->
		      </div>
		      <!--/panel-->
		    </div>
		    <!--/col--> 
  		</div>
  		<?php endforeach; 
			}
			else
			{
				echo "<a class='gidsprofiel_kiesgids' href='" . site_url('bezoeker/gidslijst') . "'>Kies een gids uit!</a>";
			}
			?>

	<?php 
	if($loggedin == 'bezoeker' || $loggedin == 'nee')
	{ ?>
	<div class="panel panel-default">
	  <div class="panel-heading">Beschikbaar op:</div>
		 <table class="table table-striped">
		 	<tr>
				<th>Datum</th>
				<th>Uur</th> 
				<th>Boek</th>
			</tr>
			<?php  
				foreach ($dates as $d => $value) : ?>
					<?php 
					echo $d['DatumId'];

					if ($value["DatumId"] == array_search($value["DatumId"], array_column($datums, 'id'))) {
						$daynumber = date("N", strtotime($datums[$value["DatumId"]]["datum"]));
							if($daynumber == 1)
							{
								$day = "Maandag";
							}
							else if ($daynumber == 2) {
								$day = "Dinsdag";
							}
							else if ($daynumber == 3) {
								$day = "Woensdag";
							}
							else if ($daynumber == 4) {
								$day = "Donderdag";
							}
							else if ($daynumber == 5) {
								$day = "Vrijdag";
							}
							else if ($daynumber == 6) {
								$day = "Zaterdag";
							}
							else if ($daynumber == 7) {
								$day = "Zondag";
							}
						$date = date("d/m/Y", strtotime($datums[$value["DatumId"]]["datum"]));
						echo "<tr><td>" . $day . ", " . $date . "</td><td>";

					} else{
						$daynumber = date("N", strtotime($datums[array_search($value["DatumId"], array_column($datums, 'id'))]["datum"]));
							if($daynumber == 1)
							{
								$day = "Maandag";
							}
							else if ($daynumber == 2) {
								$day = "Dinsdag";
							}
							else if ($daynumber == 3) {
								$day = "Woensdag";
							}
							else if ($daynumber == 4) {
								$day = "Donderdag";
							}
							else if ($daynumber == 5) {
								$day = "Vrijdag";
							}
							else if ($daynumber == 6) {
								$day = "Zaterdag";
							}
							else if ($daynumber == 7) {
								$day = "Zondag";
							}
						$date = date("d/m/Y", strtotime($datums[array_search($value["DatumId"], array_column($datums, 'id'))]["datum"]));
						echo "<tr><td>" . $day . ", " . $date . "</td><td>
						<form action='' method='post'>
						<select name='Uur' class='form-control'>
							<option value='9' selected>9 uur</option>
							<option value='11'>11 uur</option>
							<option value='13'>13 uur</option>
							<option value='15'>15 uur</option>
						</select></td>
						<td>";
						if($loggedin == 'bezoeker')
						{
						foreach ($bezoeker as $br) : 
						echo "<input style='display:none' readonly class='form-control' name='bezoekerId' value='" . $br['id'] . "'>";
						endforeach; 
						echo "<input style='display:none' readonly class='form-control' name='Datum' value='" . $datums[array_search($value["DatumId"], array_column($datums, 'id'))]["datum"] . "'>
						<input style='display:none' readonly class='form-control' name='student' value='" . $profiel_id = $this->uri->segment(3, 0) . "'>
						<button type='submit' class='btn btn-primary'>Reserveer bezoek</button></td>";
						}
						else
						{
						echo "<a href=" . site_url('gebruiker/login') . ">Log in om deze gids te boeken!</a>";
						}
						echo "</form>";
					}
					?>
				<?php endforeach; 
			?>
		
		</table>
	</div>
	<?php } ?>


		
	</div>
</body>
</html>